@extends('management.layout')
@section('title', 'Список звернень')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/management/list.css') }}">
    <link rel="stylesheet" href="{{asset('css/assets/bootstrap/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/assets/bootstrap/circle-menu.css')}}">
@endsection
@section('content')
    <div>


        <div class="row mt-3">
            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            Пошук
                        </button>
                    </h2>
                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col col-2">
                                    <div class="form-group">
                                        <label class="title-label-center label-title-100" for="number_search">
                                            <span>Номер зверення:</span>
                                            <input
                                                id="number_search"
                                                type="text"
                                                name="number"
                                                class="form-control"
                                            >
                                        </label>
                                    </div>
                                </div>
                                <div class="col col-2">
                                    <div class="form-group">
                                        <label style="position: relative;" class="title-label-center label-title-100" for="date_search">
                                            <span>Дата зверення:</span>
                                            <input
                                                id="date_search"
                                                type="text"
                                                name="date_search"
                                                class="form-control"
                                            >
                                        </label>
                                    </div>
                                </div>
                                <div class="col col-3">
                                    <div class="form-group">
                                        <label class="title-label-center label-title-100" for="full_name_search">
                                            <span>ПІБ Зверника:</span>
                                            <input
                                                id="full_name_search"
                                                type="text"
                                                name="fullname"
                                                class="form-control"
                                            >
                                        </label>
                                    </div>
                                </div>
                                <div class="col col-5">
                                    <div class="form-group">
                                        <label class="title-label-center label-title-100" for="appeal_text">
                                            <span>Короткий зміст:</span>
                                            <input
                                                id="appeal_text"
                                                type="text"
                                                name="appeal_text"
                                                class="form-control"
                                            >
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-3">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <table class="table custom-table">
                <thead>
                <tr>
                    <th width="20%" scope="col">Опис</th>
                    <th width="20%" scope="col">Питання</th>
                    <th width="25%" scope="col">Короткий зміст</th>
                    <th width="10%" scope="col">Спеціаліст</th>
                    <th width="10%" scope="col">Автор звернення</th>
                    <th width="5%" scope="col">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cards as $card)
                    <tr scope="row" class="
                    @switch($card->getStatus())
                    @case(\App\Models\Constants\Card\CardStatus::OPEN)
                        nocontrol
                    @break
                    @case(\App\Models\Constants\Card\CardStatus::CONTROL)
                        oncontrol
                    @break
                    @case(\App\Models\Constants\Card\CardStatus::CLOSED)
                        closed
                    @break
                    @endswitch
                        ">
                        <td style="vertical-align: middle">
                            <div class="row">
                                <div class="col col-6">
                                    <div>
                                        <a style="color:blue" href="#">{{$card->number}}</a>
                                    </div>
                                    <div>
                                        {{$card->incoming_date->locale('uk_UA')->isoFormat('D MMMM YYYY')}}
                                    </div>
                                    <div>
                                        @switch($card->getStatus())
                                            @case(\App\Models\Constants\Card\CardStatus::OPEN)
                                            <small class="d-block">Не на контролі</small>
                                            @break
                                            @case(\App\Models\Constants\Card\CardStatus::CONTROL)
                                            <small class="d-block">{{$card->responsible->name ?? ''}}</small>
                                            @break
                                            @case(\App\Models\Constants\Card\CardStatus::CLOSED)
                                            <small class="d-block">{{$card->responsibleClosed->name ?? ''}}</small>
                                            @break
                                        @endswitch
                                    </div>
                                </div>
                                <div class="col col-6">

                                </div>

                            </div>

                        </td>
                        <td style="vertical-align: middle">
                            @foreach($card->questions as $question)
                            <span style="color: #6b8d32">{{ $question->index }}</span>. {{ $question->title }}; <br>
                            @endforeach
                        </td>
                        <td style="vertical-align: middle">
                        <span style="font-style: italic">
                            {{$card->appeal_text_note}}
                        </span>
                        </td>
                        <td class="center_content" style="vertical-align: middle">
                            {{$card->userOpened->name ?? ''}}
                        </td>
                        <td style="vertical-align: middle" scope="col"><span
                                style="font-weight: bold">{{$card->last_name}}</span>
                            <br>{{$card->first_name}} {{$card->middle_name}}</td>
                        <td style="vertical-align: middle;">
                            <span>
                                1
                            </span>
                        </td>
                    </tr>
                    <tr class="spacer">
                        <td colspan="100"></td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12 d-flex justify-content-center">
                {{$cards->links()}}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/assets/popper.min.js')}}"></script>
    <script src="{{asset('js/assets/moment.min.js')}}"></script>
    <script src="{{asset('js/assets/moment-with-locales.js')}}"></script>
    <script src="{{asset('js/assets/bootstrap-datetimepicker.min.js')}}"></script>
    <script>
        $('#date_search').datetimepicker({
            format: 'DD.MM.YYYY',
            locale: 'uk',
        });
    </script>
@endsection
