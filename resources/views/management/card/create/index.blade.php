@extends('management.layout')
@section('title', 'Нове звернення')

@section('styles')
    <link rel="stylesheet" href="{{asset('css/management/card-style.css')}}">
    <link rel="stylesheet" href="{{asset('css/assets/bootstrap/bootstrap-datetimepicker.min.css')}}">
@endsection

@section('additional-menu-item')
    <span
        class="text-reset me-3"
        role="button"
        aria-expanded="false"
        id="search-clients"
    >
        <i class="fa fa-address-book fa-2x"></i>
    </span>
@endsection

@section('content')
    <div class="container">

        <form action="{{ route('management.card.store') }}" id="new_card_form" method="post">
            @csrf
            <div class="common_info">
                <h5 class="title-info">Загальна інформація</h5>
                <hr>
                <div class="row">
                    <div class="col col-4">
                        <div id="sign_block">
                            <div class="form-group">
                                <label class="label-field" for="sign">
                                    Ознака
                                </label>
                                <div class="d-flex justify-content-end">
                                    <select class="form-control input-align-field" name="sign" id="sign">
                                        <option value="none" selected hidden>Будь ласка, оберіть</option>
                                        @foreach($signs as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" id="sign_double" name="sign_double" value="">
                                    <span style="display: none" id="sign_edit"><i class="fa fa-edit"></i></span>
                                </div>
                                <span style="display: none" class="sign_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-4">
                        <div id="type_block">
                            <div class="form-group">
                                <label class="label-field" for="type">
                                    За типом
                                </label>
                                <select class="form-control input-align-field" name="type" id="type">
                                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                                    @foreach($types as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                <span style="display: none" class="type_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-4">
                        <div id="admission_types_block">
                            <div class="form-group">
                                <label class="label-field" for="admission_type">
                                    Надходження
                                </label>
                                <select class="form-control input-align-field" name="admission_type" id="admission_type">
                                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                                    @foreach($admission_types as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                <span style="display: none" class="admission_type_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col col-4">
                        <div id="kind_block">
                            <div class="form-group">
                                <label class="label-field" for="kind">
                                    За видами
                                </label>
                                <select class="form-control input-align-field" name="kind" id="kind">
                                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                                    @foreach($kinds as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                <span style="display: none" class="kind_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-4">
                        <div id="subject_block">
                            <div class="form-group">
                                <label class="label-field" for="subject">
                                    За суб'єктом
                                </label>
                                <select class="form-control input-align-field" name="subject" id="subject">
                                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                                    @foreach($subjects as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                <span style="display: none" class="subject_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-4">
                        <div class="row">
                            <div class="col col-6">
                                <div id="personCount_block">
                                    <div class="form-group">
                                        <label class="label-field" for="personCount">
                                            Кількість громадян
                                        </label>
                                        <input autocomplete="none" autocomplete="none"
                                               class="form-control input-align-field"
                                               type="number" disabled value="1"
                                               name="personCount" id="personCount">
                                        <span style="display: none"
                                              class="personCount_error-message error-message"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-6">
                                <div id="incomingDate_block">
                                    <div class="form-group datepicker-position">
                                        <label class="label-field" for="incomingDate">
                                            Дата надходження
                                        </label>
                                        <input autocomplete="none" autocomplete="none" type='text'
                                               class="form-control input-align-field datepicker-field" id='incomingDate'
                                               name="incomingDate"/>
                                        <span style="display: none"
                                              class="incomingDate_error-message error-message"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="citizen-info">
                <h5 class="title-info">Інформація про кореспондента</h5>
                <hr>
                <div id="first_last_name_row" class="row">
                    <div class="col col-3">
                        <div id="last_name_block">
                            <div class="form-group">
                                <label class="label-field" for="last_name">
                                    Прізвище
                                </label>
                                <input autocomplete="none" autocomplete="none" id="last_name" name="last_name"
                                       type="text"
                                       class="form-control input-align-field">
                                <span style="display: none" class="last_name_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="first_name_block">
                            <div class="form-group">
                                <label class="label-field" for="first_name">
                                    Ім'я
                                </label>
                                <input autocomplete="none" autocomplete="none" id="first_name" name="first_name"
                                       type="text"
                                       class="form-control input-align-field">
                                <span style="display: none" class="first_name_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="middle_name_block">
                            <div class="form-group">
                                <label class="label-field" for="middle_name">
                                    По-батькові
                                </label>
                                <input autocomplete="none" autocomplete="none" id="middle_name" name="middle_name"
                                       type="text"
                                       class="form-control input-align-field">
                                <span style="display: none" class="middle_name_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="gender_block">
                            <div class="form-group">
                                <label class="label-field" for="gender">
                                    Стать
                                </label>
                                <select class="form-control input-align-field" name="gender" id="gender">
                                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                                    @foreach($genders as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                <span style="display: none" class="gender_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 m-auto">
                    <span style="display: none;" id="title_is_local">
                        <span>Кореспондент не із Житомирської області</span>
                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    </span>
                </div>
                <div id="local_address_row" class="row mt-3">
                    <div class="col col-4">
                        <div id="district_block">
                            <div class="form-group">
                                <label class="label-field" for="district">
                                    Район
                                </label>
                                <div class="d-flex justify-content-between">
                                    <select class="form-control input-align-field" name="district" id="district">
                                        <option selected hidden value="none">Будь ласка, оберіть</option>
                                        <option style="display: none;" value="0">Немає даних</option>
                                        @foreach($districts as $district)
                                            <option value="{{$district->id}}">{{$district->name}}</option>
                                        @endforeach
                                    </select>
                                    <span id="no-district" class="no-address-names" data-toggle="tooltip"
                                          data-placement="top" title="Район відсутній">
                                        <i class="fa fa-times"></i>
                                    </span>
                                </div>
                                <span style="display: none" class="district_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-4">
                        <div id="otg_block">
                            {{-- ajax load --}}
                            <label class="label-field" for="otg">
                                Громада (ОТГ)
                            </label>
                            <input class="form-control input-align-field" type="text" disabled value="Оберіть район">
                        </div>
                        <div style="display: none" id="ajax_loader_otgs">
                            <img class="ajax-loader" src="{{asset('img/ajax-loader.gif')}}" alt="">
                        </div>
                    </div>
                    <div class="col col-4">
                        <div id="locality_block">
                            {{-- ajax load --}}
                            <label class="label-field" for="locality">
                                Населений пункт
                            </label>
                            <input class="form-control input-align-field" type="text" disabled value="Оберіть район">
                        </div>
                        <div style="display: none" id="ajax_loader_localities">
                            <img class="ajax-loader" src="{{asset('img/ajax-loader.gif')}}" alt="">
                        </div>
                    </div>
                </div>
                <div style="display: none;" id="not_local_address_row" class="row mt-3">
                    <div class="col col-3">
                        <div id="country_block">
                            <div class="form-group">
                                <label class="label-field" for="country">
                                    Країна
                                </label>
                                <div class="d-flex justify-content-between">
                                    <select class="form-control input-align-field" name="country" id="country">
                                        <option style="display: none;" value="0">Немає даних</option>
                                        @foreach($countries as $key => $value)
                                            <option @if($key == 1) selected
                                                    @endif value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    <span id="not_local_no-country" class="no-address-names no_local"
                                          data-toggle="tooltip"
                                          data-placement="top" title="Країна відсутня">
                                        <i class="fa fa-times"></i>
                                    </span>
                                </div>
                                <span style="display: none" class="country_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="region_other_block">
                            <div class="form-group">
                                <label class="label-field" for="region_other">
                                    Область/Край тощо
                                </label>
                                <div class="d-flex justify-content-between">
                                    <input autocomplete="none" autocomplete="none" type="text"
                                           class="form-control input-align-field" id="region_other" name="region_other">
                                    <span id="not_local_no-region_other" class="no-address-names no_local"
                                          data-toggle="tooltip"
                                          data-placement="top" title="Область відсутня">
                                    <i class="fa fa-times"></i>
                                </span>
                                </div>
                                <span style="display: none" class="region_other_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="district_other_block">
                            <div class="form-group">
                                <label class="label-field" for="district_other">
                                    Район
                                </label>
                                <div class="d-flex justify-content-between">
                                    <input autocomplete="none" autocomplete="none" type="text"
                                           class="form-control input-align-field" id="district_other"
                                           name="district_other">
                                    <span id="not_local_no-district_other" class="no-address-names no_local"
                                          data-toggle="tooltip"
                                          data-placement="top" title="Район відсутній">
                                    <i class="fa fa-times"></i>
                                </span>
                                </div>
                                <span style="display: none" class="district_other_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="locality_other_block">
                            <div class="form-group">
                                <label class="label-field" for="locality_other">
                                    Громада та населений пункт
                                </label>
                                <div class="d-flex justify-content-between">
                                    <input autocomplete="none" autocomplete="none" type="text"
                                           class="form-control input-align-field" id="locality_other"
                                           name="locality_other">
                                    <span id="not_local_no-locality_other" class="no-address-names no_local"
                                          data-toggle="tooltip"
                                          data-placement="top" title="Населений пункт відсутній">
                                    <i class="fa fa-times"></i>
                                </span>
                                </div>
                                <span style="display: none" class="locality_other_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col col-11">
                        <div id="address_block">
                            <div class="form-group">
                                <label class="label-field" for="address">
                                    Адреса
                                </label>
                                <div class="d-flex justify-content-between">
                                    <input autocomplete="none" autocomplete="none" type="text"
                                           class="form-control input-align-field" id="address"
                                           name="address">
                                    <span id="no-address" class="no-address" data-toggle="tooltip"
                                          data-placement="top" title="Адреса відсутня">
                                    <i class="fa fa-times"></i>
                                </span>
                                </div>
                                <span style="display: none" class="address_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-1">
                        <span class="is_local" id="not-from-region-icon"
                              title="Кореспондент не із Житомирської області">
                            <i class="fas fa-globe-europe fa-2x"></i>
                        </span>
                        <input style="display: none;" type="checkbox" id="local_checkbox" name="local_checkbox" checked>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col col-3">
                        <div id="zip_block">
                            <div class="form-group">
                                <label class="label-field" for="zip">
                                    Поштовий індекс
                                </label>
                                <div class="d-flex justify-content-between">
                                    <input autocomplete="none" autocomplete="none" type="text"
                                           class="form-control input-align-field"
                                           name="zip" id="zip">
                                </div>
                                <span style="display: none" class="zip_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="phone_block">
                            <div class="form-group">
                                <label class="label-field" for="phone">
                                    Номер телефону
                                </label>
                                <div class="d-flex justify-content-between">
                                    <input autocomplete="none" type="text" class="form-control input-align-field"
                                           name="phone" id="phone">
                                </div>
                                <span style="display: none" class="phone_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="phone_extra_block">
                            <div class="form-group">
                                <label class="label-field" for="phone_extra">
                                    Додатковий номер телефону
                                </label>
                                <div class="d-flex justify-content-between">
                                    <input autocomplete="none" type="text" class="form-control input-align-field"
                                           name="phone_extra" id="phone_extra">
                                </div>
                                <span style="display: none" class="phone_extra_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="email_block">
                            <div class="form-group">
                                <label class="label-field" for="email">
                                    Електронна адреса
                                </label>
                                <div class="d-flex justify-content-between">
                                    <input autocomplete="none" type="text" class="form-control input-align-field"
                                           name="email" id="email">
                                </div>
                                <span style="display: none" class="email_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col col-3">
                        <div id="social_status_block">
                            <div class="form-group">
                                <label class="label-field" for="social_status">
                                    Соціальний стан
                                </label>
                                <select class="form-control input-align-field" name="social_status"
                                        id="social_status">
                                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                                    @foreach($social_statuses as $key => $value)
                                        <option value="{{$key}}">{{$key}} {{$value}}</option>
                                    @endforeach
                                </select>
                                <span style="display: none" class="social_status_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="categoty_block">
                            <div class="form-group">
                                <label class="label-field" for="category">
                                    Категорія
                                </label>
                                <select class="form-control input-align-field" name="category" id="category">
                                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                                    @foreach($categories as $key => $value)
                                        <option value="{{$key}}">{{$key}} {{$value}}</option>
                                    @endforeach
                                </select>
                                <span style="display: none" class="category_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-6">
                        <div id="corrNote_block">
                            <div class="form-group">
                                <label class="label-field" for="corrNote">Примітка</label>
                                <input autocomplete="none" class="form-control" name="corrNote" id="corrNote">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="organization-info">
                <h5 class="title-info">Інформація про організацію-кореспондента</h5>
                <hr>
                <div class="row">
                    <div class="col col-12">
                        <div id="from_block">
                            <div class="form-group">
                                <label class="label-field" for="from">
                                    Звідки одержано
                                </label>
                                <select class="form-control input-align-field" name="admission" id="from">
                                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                                    @foreach($admissions as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                <span style="display: none" class="from_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3" id="corr_date_row">
                    <div class="col col-3">
                        <div id="orgDate_block">
                            <div class="form-group datepicker-position">
                                <label class="label-field" for="orgDate">
                                    Дата надіслання
                                </label>
                                <input autocomplete="none" type='text'
                                       class="form-control input-align-field datepicker-field" id='orgDate'
                                       name="orgDate"/>
                                <span style="display: none" class="orgDate_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-3">
                        <div id="orgIndex_block">
                            <div class="form-group datepicker-position">
                                <label class="label-field" for="orgIndex">
                                    Індекс документа
                                </label>
                                <input autocomplete="none" type='text'
                                       class="form-control input-align-field" id='orgIndex'
                                       name="orgIndex"/>
                                <span style="display: none" class="orgIndex_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-6">
                        <div id="orgNote_block">
                            <div class="form-group">
                                <label class="label-field" for="orgNote">Примітка</label>
                                <input autocomplete="none" class="form-control" name="orgNote" id="orgNote">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col col-6">
                        <div id="question_block">
                            <div class="form-group">
                                <label class="label-field" for="question_0">
                                    Питання
                                </label>
                                <div class="d-flex justify-content-between">

                                    <select class="form-control input-align-field questions" name="questions[]"
                                            id="question_0" data-row-id="0">
                                        <option value="none" selected hidden>Будь ласка, оберіть</option>
                                        @foreach($questions as $question)
                                            <option value="{{$question->id}}">{{$question->index}} {{$question->title}}</option>
                                        @endforeach
                                    </select>
                                    <span id="add_question_btn" data-row-number="0" data-toggle="tooltip" data-placement="top"
                                          title="Додати питання">
                                        <i class="fa fa-plus-circle"></i>
                                    </span>
                                </div>
                                <span style="display: none" class="question_0_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-6">
                        <div id="subquestion_block_0" data-subquestion-row="0">
                            {{-- ajax load --}}
                            <div class="form-group">
                                <label class="label-field" for="subquestion_0">
                                    Підпитання
                                </label>
                                <input autocomplete="none" class="form-control input-align-field subquestions"
                                       id="subquestion_0" type="text" disabled
                                       value="Оберіть питання">
                            </div>
                        </div>
                        <div style="display: none" id="ajax_loader_subquestions_0">
                            <img class="ajax-loader" src="{{asset('img/ajax-loader.gif')}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="row mt-3 text_appeal_row">
                    <div id="text_appeal_block">
                        <div class="form-group">
                            <label class="label-field" for="appeal_text">
                                Текст звернення
                            </label>
                            <textarea class="form-control" name="appeal_text" id="appeal_text" cols="30"
                                      rows="5"></textarea>
                            <span style="display: none" class="appeal_text_error-message error-message"></span>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div id="doer_block">
                        <div class="form-group">
                            <label class="label-field" for="doer_0">
                                Виконавець
                            </label>
                            <div class="d-flex justify-content-between">
                                <select class="form-control input-align-field" name="doers[]" id="doer_0">
                                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                                    @foreach($doers as $doer)
                                        <option value="{{$doer->id}}">{{$doer->name}}
                                            @if(strlen($doer->organization))
                                                ({{$doer->organization}})
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                                <span id="add_doer_btn" data-toggle="tooltip" data-placement="top"
                                      title="Додати виконавця" data-row-number="0">
                                    <i class="fa fa-plus-circle"></i>
                            </span>
                            </div>
                            <span style="display: none" class="doer_0_error-message error-message"></span>
                        </div>
                    </div>
                </div>
                <div class="row mt-3" id="text_resolution_row">
                    <div class="col col-4">
                        <div class="d-flex flex-column">
                            <div id="responsible_block">
                                <div class="form-group">
                                    <label class="label-field" for="responsible">
                                        Відповідальна особа
                                    </label>
                                    <select class="form-control input-align-field" name="responsible" id="responsible">
                                        <option value="none" selected hidden>Будь ласка, оберіть</option>
                                        @foreach($responsibles as $responsible)

                                            <option value="{{$responsible->id}}">{{$responsible->name}}
                                                ({{$responsible->responsiblePosition->title}})

                                            </option>
                                        @endforeach
                                    </select>
                                    <span style="display: none" class="responsible_error-message error-message"></span>
                                </div>
                            </div>
                            <div class="mt-3" id="term_block">
                                <div class="form-group datepicker-position">
                                    <label class="label-field" for="term_date">
                                        Термін виконання
                                    </label>
                                    <input autocomplete="none" type='text'
                                           class="form-control input-align-field datepicker-field" id='term_date'
                                           name="term_date"/>
                                    <span style="display: none" class="term_date_error-message error-message"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-7">
                        <div id="text_appeal_block">
                            <div class="form-group">
                                <div class="d-flex justify-content-center">
                                    <label class="label-field" for="resolution_text">
                                        Текст резолюції
                                    </label>
                                </div>
                                <textarea class="form-control" name="resolution_text" id="resolution_text" cols="30"
                                          rows="4"></textarea>
                                <span style="display: none" class="resolution_text_error-message error-message"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col col-1">
                        <div id="pattern_resolution_block">
                            <i id="pattern_resolution" class="fa fa-file-text"
                               title="Шаблони резолюцій"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="d-flex justify-content-center mt-3">
                        <button type="submit" id="submit-btn" class="btn btn-primary">Зберегти</button>
                    </div>
                </div>
            </div>
        </form>

        @include('management.card.create.modals.double_appeal')
        @include('management.card.create.modals.patterns_resolution')
        @include('management.card.create.modals.confirm_auto_fill')
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/assets/popper.min.js')}}"></script>
    <script src="{{asset('js/assets/moment.min.js')}}"></script>
    <script src="{{asset('js/assets/moment-with-locales.js')}}"></script>
    <script src="{{asset('js/assets/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/assets/moment_functions.js')}}"></script>
    <script src="{{asset('js/assets/custom_additional_validate_methods.js')}}"></script>
    <script src="{{asset('js/assets/form_rules.js')}}"></script>


    <script src="{{asset('js/assets/bootstrap-datetimepicker.min.js')}}"></script>



    <script>
        $(document).ready(function () {

            $(document).on('click', '#local_checkbox', function (){
                alert(1);
            });
            $(document).on('click', '#search-clients', function () {
                alert(1);
            });

            $('#new_card_form').validate(
                formRules
            );
            // $(document).on('submit', '#new_card_form', function(event){
            //     event.preventDefault();
            // });

            $(document).on('focusout', '.questions, .subquestions', function () {
                validateAdditional(this);
            });


            function validateAdditional(element) {
                let value = $(element).val();
                if (value == 'none') {
                    let elementId = $(element).attr('id');
                    $(element).addClass('error-input').removeClass('success-input');
                    $('.' + elementId + "_error-message").text("Обов'язково до заповнення").show();
                } else {
                    let elementId = $(element).attr('id');
                    $(element).removeClass('error-input').addClass('success-input');
                    $('.' + elementId + "_error-message").hide();
                }
            }

            $('.datepicker-field').datetimepicker({
                format: 'DD.MM.YYYY',
                locale: 'uk',
            });

            $(document).on('change', '#sign', function () {
                if ($(this).val() == '{{\App\Models\Constants\Card\CardSign::DOUBLE}}') {
                    $('#double_appeal_modal').modal('show');
                } else if($(this).val() == '{{\App\Models\Constants\Card\CardSign::REPEATED}}'){
                    $('#double_appeal_modal').modal('show');
                }else {
                    $('#sign_edit').hide();
                }
            });

            $(document).on('change keyup', '#number_for_double', function () {
                let searchText = $(this).val();
                if (searchText.length > 0) {
                    getCardsForDouble(searchText);
                }
            });

            $(document).on('change', '#from', function () {
                let selected = $(this).val();
                if (selected == '{{ \App\Models\Constants\Card\CardAdmission::LOCAL }}') {
                    $('#corr_date_row').hide();
                } else {
                    $('#corr_date_row').show();
                }
            });

            $(document).on('click', '.no-address-names', function () {
                let icon_id = $(this).attr('id');
                let input_field_id;
                let no_local = $(this).hasClass('no_local');
                if (!no_local) {
                    input_field_id = '#' + icon_id.replace('no-', '');
                } else {
                    input_field_id = '#' + icon_id.replace('not_local_no-', '');
                }
                if ($(this).find('i').hasClass('fa-times')) {
                    if (input_field_id == '#district') {
                        localStorage.setItem('district_id_edit', $(input_field_id).val());
                    }
                    if (input_field_id == '#otg') {
                        localStorage.setItem('otg_id_edit', $(input_field_id).val());
                    }
                    if (input_field_id == '#locality') {
                        localStorage.setItem('locality_id_edit', $(input_field_id).val());
                    }
                    if (input_field_id == '#country') {
                        localStorage.setItem('country_edit', $(input_field_id).val());
                    }
                    if (input_field_id == '#region_other') {
                        localStorage.setItem('region_edit', $(input_field_id).val());
                    }
                    if (input_field_id == '#district_other') {
                        localStorage.setItem('district_edit', $(input_field_id).val());
                    }
                    if (input_field_id == '#locality_other') {
                        localStorage.setItem('locality_edit', $(input_field_id).val());
                    }
                    $(this).find('i').removeClass('fa-times').addClass('fa-edit');
                    if (!no_local) {
                        $(input_field_id).attr('disabled', true).addClass('disabled').val(0);
                    } else {
                        if ($(input_field_id).prop("tagName") == 'INPUT') {
                            $(input_field_id).attr('disabled', true).addClass('disabled').val('Немає даних');
                        } else {
                            $(input_field_id).attr('disabled', true).addClass('disabled').val(0);
                        }
                    }
                    $(input_field_id).removeClass('error-input').removeClass('success-input');
                    let classError = input_field_id.replace('#', '.');
                    $(classError + "_error-message").hide();

                } else {
                    $(this).find('i').removeClass('fa-edit').addClass('fa-times');
                    $(input_field_id).attr('disabled', false).removeClass('disabled');
                    if (input_field_id == '#district') {
                        $(input_field_id).val(localStorage.getItem('district_id_edit'));
                        localStorage.removeItem('district_id_edit');
                    }
                    if (input_field_id == '#otg') {
                        $(input_field_id).val(localStorage.getItem('otg_id_edit'));
                        localStorage.removeItem('otg_id_edit');
                    }
                    if (input_field_id == '#locality') {
                        $(input_field_id).val(localStorage.getItem('locality_id_edit'));
                        localStorage.removeItem('locality_id_edit');
                    }
                    if (input_field_id == '#country') {
                        $(input_field_id).val(localStorage.getItem('country_edit'));
                        localStorage.removeItem('country_edit');
                    }
                    if (input_field_id == '#region_other') {
                        $(input_field_id).val(localStorage.getItem('region_edit'));
                        localStorage.removeItem('region_edit');
                    }
                    if (input_field_id == '#district_other') {
                        $(input_field_id).val(localStorage.getItem('district_edit'));
                        localStorage.removeItem('district_edit');
                    }
                    if (input_field_id == '#locality_other') {
                        $(input_field_id).val(localStorage.getItem('locality_edit'));
                        localStorage.removeItem('locality_edit');
                    }
                }
            });

            $(document).on('click', '#no-address', function () {
                if ($(this).find('i').hasClass('fa-times')) {
                    localStorage.setItem('address_edit', $('#address').val());
                    $(this).find('i').removeClass('fa-times').addClass('fa-edit');
                    $('#address').removeClass('error-input').removeClass('success-input');
                    let classError = ".address";
                    $(classError + "_error-message").hide();
                    $('#address').attr('disabled', true).addClass('disabled').val('Адреса відсутня');
                } else {
                    $(this).find('i').addClass('fa-times').removeClass('fa-edit');
                    $('#address').attr('disabled', false).removeClass('disabled');
                    $('#address').val(localStorage.getItem('address_edit'));
                }
            });

            $(document).on('change', '#subject', function () {
                if ($(this).val() == '{{\App\Models\Constants\Card\CardSubject::ANONYMOUS}}') {
                    $('#first_last_name_row').hide();
                } else {
                    $('#first_last_name_row').show();
                }
                if ($(this).val() == '{{\App\Models\Constants\Card\CardSubject::GROUP}}') {
                    $('#personCount').attr('disabled', false);
                } else {
                    $('#personCount').val(1).attr('disabled', true);
                }
            });

            $('#district').change(function () {
                let district_id = $(this).val();
                getOtgs(district_id);
            });

            $(document).on('change', '#otg', function () {
                let otg_id = $(this).val();
                getLocalities(otg_id);
            });
            $('#list_for_double_ajax').on('dblclick', '.ajax-double-appeal-row', function (event) {
                let cardNumber = $(this).data('card');
                let cardId = $(this).data('card-id');
                $('#sign_double').val(cardNumber);
                $('#autofill_card_id').val(cardId);
                $('#auto_fill_confirm').modal('show');
                updateSign(cardNumber);
            });

            $(document).on('click', '#sign_edit', function () {
                $('#double_appeal_modal').modal('show');
            });

            $(document).on('click', '#not-from-region-icon', function () {
                isLocalAction(this);
            });

            $(document).on('change', '.questions', function () {
                $(this).attr('disabled', true);
                let questionId = $(this).val();
                let row_id = $(this).data('row-id');
                getSubquestions(questionId, row_id);
            });

            $(document).on('click', '#add_question_btn', function () {
                let rowNumber = $(this).data('row-number');
                getQuestions(rowNumber);
            });

            $(document).on('click', '.remove_question_btn', function () {
                let rowNumber = $(this).data('row-number');
                let rowToRemove = $('#row_' + rowNumber);
                //rowToRemove.addClass('remove_question_btn_animation');
                rowToRemove.remove();
            });

            $(document).on('click', '#add_doer_btn', function () {
                let rowNumber = $(this).data('row-number');
                getDoer(rowNumber);
            });

            $(document).on('click', '.remove_doer_btn', function () {
                let rowNumber = $(this).data('row-number');
                $('#doer_row_' + rowNumber).remove();
            });
            $(document).on('click', '#pattern_resolution', function () {
                $('#patterns_resolution_modal').modal('show');
            });
            $(document).on('click', '.close_modal_pattern', function () {
                $('#patterns_resolution_modal').modal('hide');
            });
            $(document).on('click', '.resolution_item', function () {
                let resolutionText = $(this).text();
                $('#resolution_text').empty();
                $('#resolution_text').val(resolutionText.trim());
                $('#patterns_resolution_modal').modal('hide');
                $('#resolution_text').trigger('click change').blur();
            });
            $(document).on('click', '#autofill_yes', function () {
                let cardId = $('#autofill_card_id').val();
                $('#auto_fill_confirm').modal('hide');
                {{--let url = "{{ route('card.get-autofill', ':id') }}";--}}
                url = url.replace(':id', cardId);
                $.ajax({
                    url: url,
                    type: "GET",
                    data: {
                        cardId: cardId,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        if (response.success) {
                            autoFill(response)
                        }
                    },
                    complete: function () {
                    }
                });
            });
        });
        function isLocalAction(icon, autofill = false){
            if ($(icon).hasClass('is_local') || autofill) {
                $('#title_is_local').show();
                $('#local_checkbox').attr('checked', false);

                $('#district').removeClass('error-input');
                $('.district_error-message').hide();
                $('#otg').removeClass('error-input');
                $('.otg_error-message').hide();
                $('#locality').removeClass('error-input');
                $('.locality_error-message').hide();



                $('#local_address_row').fadeOut(250, 'linear', function () {
                    $('#not_local_address_row').fadeIn(250, 'linear');
                });
                $(icon).removeClass('is_local').attr('title', 'Кореспондент з Житомирської області');
            } else {
                $('#title_is_local').hide();
                $('#local_checkbox').attr('checked', true);

                $('#country').removeClass('error-input');
                $('.country_error-message').hide();
                $('#district_other').removeClass('error-input');
                $('.district_other_error-message').hide();
                $('#region_other').removeClass('error-input');
                $('.region_other_error-message').hide();
                $('#locality_other').removeClass('error-input');
                $('.locality_other_error-message').hide();

                $('#not_local_address_row').fadeOut(250, 'linear', function () {
                    $('#local_address_row').fadeIn(250, 'linear');
                });
                $(icon).addClass('is_local').attr('title', 'Кореспондент не із Житомирської області');
            }
        }
        function autoFill(data) {
            $('.additional').remove();
            let signOptionText = $('#sign :selected').text();
            let signOptionVal = $('#sign :selected').val();
            $('#new_card_form').trigger('reset');
            $('#sign').val(signOptionVal);
            $('#sign :selected').text(signOptionText);
            for (let [key, value] of Object.entries(data)) {
                let index_q = 0;
                if (key == 'questions') {
                    for (let [question_id, subquestion_id] of Object.entries(value)) {
                        if(index_q > 0){
                            getQuestions(index_q - 1, question_id);
                        }
                        $(document).find('#question_' + index_q).val(question_id);
                        getSubquestions(question_id, index_q, subquestion_id);
                        ++index_q;
                    }

                }
                if (key == 'doers'){
                    let index_d = 0;
                    for (let [doer_id, doer_value] of Object.entries(value)){
                        if(index_d > 0){
                            getDoer(index_d - 1, doer_value);
                        }
                        $(document).find('#doer_' + index_d).val(doer_value);
                        ++index_d;
                    }
                }
                if(key == 'district'){
                    getOtgs(value, data['otg'], data['locality']);
                    let element = $('#' + key);
                    element.val(value);
                    continue;
                }
                if (key == 'otg' || key == 'locality') continue;

                if(key == 'local_checkbox'){
                    if(value == 0){
                        isLocalAction('#not-from-region-icon', true);
                    }else {
                        $('#' + key).val('on');
                    }
                    continue;
                }
                if(value == null && $('#' + key).next().find('i').hasClass('fa-times')){
                    $('#' + key).next().trigger('click');
                    continue;
                }
                let element = $('#' + key);
                element.val(value);
                element.trigger('change').blur();
            }
            $('#new_card_form').valid();
        }

        ////////////////////////
        //START [for double handle]
        ////////////////////////


        function updateSign(number) {
            let option;
            let optionText;
            if($('#sign').val() == "{{\App\Models\Constants\Card\CardSign::DOUBLE}}"){
                option = $('#sign option[value="{{\App\Models\Constants\Card\CardSign::DOUBLE}}"]');
                optionText = option.text();
                if (optionText != 'Дублетне')
                    optionText = 'Дублетне';
            }else if($('#sign').val() == "{{\App\Models\Constants\Card\CardSign::REPEATED}}"){
                option = $('#sign option[value="{{\App\Models\Constants\Card\CardSign::REPEATED}}"]');
                optionText = option.text();
                if (optionText != 'Повторне')
                    optionText = 'Повторне';
            }
            option.text(optionText + " " + number);
            $('#sign_edit').show();
            $('#double_appeal_modal').modal('hide');
        }

        ////////////////////////
        //END [for double handle]
        ////////////////////////

        $(document).on('hide.bs.modal', '#double_appeal_modal', function () {
            let sign = $('#sign');
            let signOptionText = $('#sign :selected').text();
            if (sign.val() == '{{\App\Models\Constants\Card\CardSign::DOUBLE}}' && signOptionText == 'Дублетне') {
                sign.val("none");
            }
        });


        ////////////////////////
        //START [Get for double]
        ////////////////////////

        function getCardsForDouble(numberOrLastName) {
            $.ajax({
                {{--url: "{{ route('cards-for-double') }}",--}}
                type: "GET",
                data: {
                    numberOrLastName: numberOrLastName,
                    _token: "{{ csrf_token() }}",
                },
                success: function (response) {
                    if (response.success) {
                        $('#list_for_double_ajax').html(response.html);

                    } else {
                        $('#list_for_double_ajax').html('<p>Нічого не знайдено</p>');
                    }
                },
                complete: function () {
                }
            });
        }

        ///////////////////////
        //END [Get for double]
        ///////////////////////


        ////////////////////////
        //START [Get otgs]
        ////////////////////////

        function getOtgs(district_id, otg_id = null, locality_id = null) {
            $('#ajax_otgs_block').hide();
            $('#ajax_loader_otgs').show();
            $('#ajax_localities_block').hide();
            $('#ajax_loader_localities').show();
            let data = {
                district_id: district_id,
            };
            if(otg_id){
                Object.assign(data, {
                    otg_id: otg_id,
                });
            }
            $.ajax({
                url: "{{ route('management.card.get-union-communities') }}",
                type: "GET",
                data: data,
                success: function (response) {
                    if (response.success) {
                        $('#otg_block').html(response.html);
                        locality_id ? getLocalities(otg_id, locality_id): getLocalities(response.first_otg_id, locality_id);
                    }
                },
                complete: function () {
                    $('#ajax_loader_otgs').hide();
                    $('#ajax_otgs_block').show();
                    $('#otg').attr('disabled', true);
                }
            });
        }

        ///////////////////////
        //END [Get otgs]
        ///////////////////////

        ////////////////////////
        //START [Get localities]
        ////////////////////////

        function getLocalities(otg_id, locality_id = null) {
            $('#ajax_localities_block').hide();
            $('#ajax_loader_localities').show();
            let data = {
                otg_id: otg_id,
                _token: "{{ csrf_token() }}",
            };
            if(locality_id){
                Object.assign(data, {
                    locality_id: locality_id,
                });
            }
            $.ajax({
                url: "{{ route('management.card.get-localities') }}",
                type: "GET",
                data: data,
                success: function (response) {
                    if (response.success) {
                        $('#locality_block').html(response.html);
                    }
                },
                complete: function () {
                    $('#ajax_loader_localities').hide();
                    $('#ajax_localities_block').show();
                    $('#otg').attr('disabled', false);
                }
            });
        }

        ///////////////////////
        //END [Get Localities]
        ///////////////////////

        ////////////////////////
        //START [Get Questions]
        ////////////////////////
        function getQuestions(row_id, questionId = null){
            let data = {
                _token: "{{ csrf_token() }}",
                rowNumber: row_id,
            };
            if (questionId){
                Object.assign(data, {
                    question_id: questionId,
                });
            }
            $.ajax({
                url: "{{ route('management.card.get-questions') }}",
                type: "GET",
                data: data,
                success: function (response) {
                    if (response.success) {
                        $('.text_appeal_row').before(response.html);
                        $('#add_question_btn').data('row-number', response.rowNumber);
                        window.scrollTo(10, $("#row_" + response.rowNumber).offset().top);
                    }
                },
                complete: function () {

                }
            });
        }
        ///////////////////////
        //END [Get Questions]
        ///////////////////////

        ////////////////////////
        //START [Get Subquestions]
        ////////////////////////

        function getSubquestions(question_id, row_id, subquestion_id = null) {
            $('#subquestion_id_block').hide();
            $('#ajax_loader_subquestions_' + row_id).show();
            let data = {
                question_id: question_id,
                row_id: row_id,
                _token: "{{ csrf_token() }}",
            };

            if (subquestion_id){
                Object.assign(data, {
                    subquestion_id: subquestion_id,
                });
            }
            $.ajax({
                url: "{{ route('management.card.get-subquestions') }}",
                type: "GET",
                data: data,
                success: function (response) {
                    if (response.success) {
                        $('#subquestion_block_' + row_id).html(response.html);
                    }
                },
                complete: function () {
                    $('#ajax_loader_subquestions_' + row_id).hide();
                    $('#subquestion_block_' + row_id).show();
                    $('#question_' + row_id).attr('disabled', false);
                }
            });
        }

        ///////////////////////
        //END [Get Subquestions]
        ///////////////////////
        function getDoer(row_id, doer_id = null){
            let data = {
                _token: "{{ csrf_token() }}",
                rowNumber: row_id,
            };
            if(doer_id){
                Object.assign(data, {
                    doer_id:doer_id,
                });
            }
            $.ajax({
                url: "{{ route('management.card.get-doers') }}",
                type: "GET",
                data: data,
                success: function (response) {
                    if (response.success) {
                        $('#text_resolution_row').before(response.html);
                        $('#add_doer_btn').data('row-number', response.rowNumber);
                        window.scrollTo(10, $("#doer_row_" + response.rowNumber).offset().top);
                    }
                },
                complete: function () {

                }
            });
        }
    </script>
@endsection
