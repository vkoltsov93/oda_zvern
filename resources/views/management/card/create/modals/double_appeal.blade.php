<div id="double_appeal_modal" class="modal fade" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Номер попереднього звернення</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col col-2">
                        <label class="modal-label" for="number_for_double">Номер:</label>
                    </div>
                    <div class="col col-10">
                        <input id="number_for_double" class="form-control" type="text">
                    </div>
                </div>
                <hr>
                <div id="list_for_double_ajax">
                    {{-- ajax load --}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary modal-close" id="close_modal_double">Закрити</button>
            </div>
        </div>
    </div>
</div>
