@isset($cardsForDouble)
    <div class="double_modal_list">
        <div class="row">
            <div class="col col-3">
                Номер
            </div>
            <div class="col col-3">
                Прізвище
            </div>
            <div class="col col-3">
                Дата
            </div>
            <div class="col col-3">
                Питання
            </div>
        </div>
        <hr>
        @foreach($cardsForDouble as $card)
            <div class="row ajax-double-appeal-row" data-card-id="{{$card->id}}" data-card="{{$card->number}}">
                <div class="col col-3">
                    {{$card->number}}
                </div>
                <div class="col col-3">
                    {{$card->last_name}}
                </div>
                <div class="col col-3">
                    {{$card->incoming_date}}
                </div>
                <div class="col col-3">
                    @foreach($card->subquestions as $subquestion)
                        {{$subquestion->question->title}}; <br>
                        <hr>
                    @endforeach
                </div>
            </div>
            <hr>
        @endforeach
    </div>
@endisset
