<div class="modal fade" id="auto_fill_confirm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Автоматичне заповнення</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Заповнити дані з попереднього звернення?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Ні</button>
                <button id="autofill_yes" type="button" class="btn btn-primary">Так</button>
            </div>
            <input type="hidden" id="autofill_card_id">
        </div>
    </div>
</div>
