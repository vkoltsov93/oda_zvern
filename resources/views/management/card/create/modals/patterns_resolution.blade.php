<div id="patterns_resolution_modal" class="modal fade" tabindex="-1" aria-labelledby="patterns_resolution_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Резолюції</h5>
                <button type="button" class="btn-close close_modal_pattern" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                @foreach($resolutions as $resolution)
                    <div class="row">
                        <div id="resolution_{{ $resolution->id }}" class="col col-12 resolution_item">
                            {{ $resolution->content }}
                        </div>
                    </div>

                    @if(!$loop->last)
                        <hr>
                    @endif
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close_modal_pattern">Закрити</button>
            </div>
        </div>
    </div>
</div>
