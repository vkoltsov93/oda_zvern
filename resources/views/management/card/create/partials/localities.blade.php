<div id="ajax_localities_block" class="form-group">
    <label class="label-field" for="locality">
        Населений пункт
    </label>
    <div class="d-flex justify-content-between">
        <select class="form-control input-align-field" name="locality" id="locality">
            <option selected hidden value="none">Будь ласка, оберіть</option>
            <option style="display: none;" value="0">Немає даних</option>
            @foreach($localities as $locality)
                <option @if(isset($cardLocalityId) && $cardLocalityId == $locality->id) selected @endif value="{{$locality->id}}">{{$locality->name}}</option>
            @endforeach
        </select>
        <span id="no-locality" class="no-address-names" data-toggle="tooltip" data-placement="top" title="Населений пункт відсутній">
         <i class="fa fa-times"></i>
        </span>
    </div>
    <span style="display: none" class="locality_error-message error-message"></span>
</div>
