<div class="row mt-3 additional-question-row additional" id="row_{{ $rowNumber }}">
    <div class="col col-6">
        <div class="form-group">
            <label
                class="label-field"
                for="question_{{ $rowNumber }}">
                Питання
            </label>
            <div class="d-flex justify-content-between">

                <select
                    class="form-control input-align-field questions"
                    name="questions[]"
                    id="question_{{ $rowNumber }}"
                    data-row-id="{{ $rowNumber }}"
                >
                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                    @foreach($questions as $question)
                        <option @if(isset($cardQuestionId) && $cardQuestionId == $question->id) selected @endif value="{{$question->id}}">{{$question->id}} {{$question->title}}</option>
                    @endforeach
                </select>
                <span
                    class="remove_question_btn"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Видалити питання"
                    data-row-number="{{ $rowNumber }}"
                >
                                        <i class="fa fa-minus-circle"></i>
                                    </span>
            </div>
            <span style="display: none" class="question_{{ $rowNumber }}_error-message error-message">Обов'язково до заповнення</span>
        </div>
    </div>
    <div class="col col-6">
        <div id="subquestion_block_{{ $rowNumber }}">
            <div class="form-group">
                <label class="label-field" for="subquestion_{{ $rowNumber }}">
                    Підпитання
                </label>
                <input class="form-control input-align-field" id="subquestion_{{ $rowNumber }}" type="text" disabled value="Оберіть питання">
            </div>
        </div>
        <div style="display: none" id="ajax_loader_subquestions_0">
            <img class="ajax-loader" src="{{asset('img/ajax-loader.gif')}}" alt="">
        </div>
    </div>
</div>
