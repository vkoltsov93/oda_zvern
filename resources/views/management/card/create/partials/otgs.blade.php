<div id="ajax_otgs_block" class="form-group">
    <label class="label-field" for="otg">
        Громада (ОТГ)
    </label>
    <div class="d-flex justify-content-between">
        <select class="form-control input-align-field" name="union_community" id="otg">
            <option selected hidden value="none">Будь ласка, оберіть</option>
            <option style="display: none;" value="0">Немає даних</option>
            @foreach($otgs as $unionCommunity)
                <option @if(isset($cardOtgId) && $cardOtgId == $unionCommunity->id) selected @endif value="{{$unionCommunity->id}}">{{$unionCommunity->name}}</option>
            @endforeach
        </select>
        <span id="no-otg" class="no-address-names" data-toggle="tooltip" data-placement="top" title="ОГТ відсутне">
         <i class="fa fa-times"></i>
    </span>
    </div>
    <span style="display: none" class="otg_error-message error-message"></span>
</div>
