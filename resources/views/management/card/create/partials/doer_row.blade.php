<div class="row mt-3 additional" id="doer_row_{{ $rowNumber }}">
    <div id="doer_block">
        <div class="form-group">
            <label class="label-field" for="doer_block">
                Виконавець
            </label>
            <div class="d-flex justify-content-between">
                <select
                    class="form-control input-align-field"
                    name="doers[]"
                    id="doer_{{ $rowNumber }}"

                >
                    <option value="none" selected hidden>Будь ласка, оберіть</option>
                    @foreach($doers as $doer)
                        <option @if(isset($cardDoerId) && $cardDoerId == $doer->id) selected @endif value="{{$doer->id}}">{{$doer->name}} ({{$doer->organization}})</option>
                    @endforeach
                </select>
                <span class="remove_doer_btn"
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Додати виконавця"
                      data-row-number="{{ $rowNumber }}"
                >
                                    <i class="fa fa-minus-circle"></i>
                            </span>
            </div>
            <span style="display: none" class="doer_{{ $rowNumber }}_error-message error-message">Обов'язково до заповнення</span>
        </div>
    </div>
</div>
