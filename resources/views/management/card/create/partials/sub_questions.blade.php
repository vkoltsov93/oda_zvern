<div class="form-group">
    <label class="label-field" for="sub_question_{{ $rowNumber }}">
        Підпитання
    </label>
    <select class="form-control input-align-field subquestions" name="sub_questions[]" id="sub_question_{{ $rowNumber }}">
        <option value="none" selected hidden>Будь ласка, оберіть</option>
        @foreach($subquestions as $subquestion)
            <option @if(isset($cardSubQuestionId) && $cardSubQuestionId == $subquestion->id) selected @endif value="{{$subquestion->id}}">{{$subquestion->index}} {{$subquestion->title}}</option>
        @endforeach
    </select>
    <span style="display: none" class="sub_question_{{ $rowNumber }}_error-message error-message"></span>
</div>
