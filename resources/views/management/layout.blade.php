<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'Dashboard')</title>
    <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('css/assets/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/assets/font-awesome/all-fas.css')}}">
    <link rel="stylesheet" href="{{asset('css/management/styles_for_nav.css')}}">

    @yield('styles')
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <!-- Container wrapper -->
    <div class="container">
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
        <!-- Toggle button -->
        <button
            class="navbar-toggler"
            type="button"
            data-mdb-toggle="collapse"
            data-mdb-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
            <i class="fas fa-bars"></i>
        </button>

        <!-- Collapsible wrapper -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Navbar brand -->
            <a class="navbar-brand mt-2 mt-lg-0" href="{{route('management.dashboard.index')}}">
                <div id="logo-block">
                    <img
                        id="logo-nav"
                        src="{{asset('img/logo_zoda.svg')}}"
                        height="15"
                        alt=""
                        loading="lazy"
                    />
                </div>
            </a>
            <!-- Left links -->
            @if(\Illuminate\Support\Facades\Auth::check())
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('management.dashboard.index')}}">Головна</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('management.card.list') }}">Список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Довідники</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Налаштування</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Звіти</a>
                    </li>

                </ul>
            @endif
            <!-- Left links -->
        </div>
        <!-- Collapsible wrapper -->

        <!-- Right elements -->
    @yield('additional-menu-item')
    <!-- Notifications -->
        <!-- Right elements -->
        @if(\Illuminate\Support\Facades\Auth::check())
            <div class="dropdown dropstart">
                <a class="" id="dropdownNav" data-bs-toggle="dropdown" aria-expanded="false">
                    <img
                        id="user-photo-nav"
                        src="https://mdbootstrap.com/img/new/avatars/2.jpg"
                        class="rounded-circle"
                        height="25"
                        alt=""
                        loading="lazy"
                    />
                </a>
                <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownNav">
                    <li><a class="dropdown-item disabled">{{Auth::user()->name}}</a></li>
                    <li><a class="dropdown-item" href="#">Налаштування</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                        >Вийти</a></li>
                </ul>
            </div>
        @endif
    </div>
    <!-- Container wrapper -->

</nav>
<div class="container">
    @yield('content')
</div>
</body>
    <script src="{{asset('js/assets/jquery-3.6.0.min.js')}}"></script>
    <script src="{{asset('js/assets/bootstrap/bootstrap.js')}}"></script>
    @yield('scripts')
</html>
