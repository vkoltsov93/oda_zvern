<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::get('/', function () {
    return redirect()->route('management.dashboard.index');
});


Route::group(['middleware' => 'auth'], function (){
    Route::group(['as' => 'management.'], function (){

        //Dashboard
        Route::group(['as' => 'dashboard.', 'prefix' => 'dashboard'], function (){
            Route::get('/', [\App\Http\Controllers\Management\DashboardController::class, 'index'])->name('index');
        });

        //Cards
        Route::group(['as' => 'card.', 'prefix' => 'card'], function (){
            Route::get('list', [\App\Http\Controllers\Management\CardController::class, 'list'])->name('list');
            Route::get('create', [\App\Http\Controllers\Management\CardController::class, 'create'])->name('create');
            Route::post('store', [\App\Http\Controllers\Management\CardController::class, 'store'])->name('store');
            Route::get('get-questions', [\App\Http\Controllers\Management\CardController::class, 'getQuestions'])->name('get-questions');
            Route::get('get-subquestions', [\App\Http\Controllers\Management\CardController::class, 'getSubquestions'])->name('get-subquestions');
            Route::get('get-doers', [\App\Http\Controllers\Management\CardController::class, 'getDoers'])->name('get-doers');
            Route::get('get-union-communities', [\App\Http\Controllers\Management\CardController::class, 'getUnionCommunities'])->name('get-union-communities');
            Route::get('get-localities', [\App\Http\Controllers\Management\CardController::class, 'getLocalities'])->name('get-localities');
//            Route::get('')
        });

    });
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
