function diffDateYears(date, format) {
    let dateStart = moment(date, format);
    let dateNow = moment();
    let duration = moment.duration(dateNow.diff(dateStart));

    return duration.years();
}

function diffDateMonths(date, format) {
    let dateStart = moment(date, format);
    let dateNow = moment();
    let duration = moment.duration(dateNow.diff(dateStart));

    return (duration.years() * 12 + duration.months());
}
function diffDateDays(date, format) {
    let dateStart = moment(date, format);
    let dateNow = moment();
   return (dateStart.diff(dateNow, 'days'));
}
function diffDatePartDays(date, format) {
    let dateStart = moment(date, format);
    let dateNow = moment();
    return (dateStart.diff(dateNow, 'hours') / 24);
}


function checkFormatDateShort(date) {
    let format = 'MM.YYYY';
    return moment(date, format).format(format) === date;
}

function checkFormatDateFull(date) {
    let format = 'DD.MM.YYYY';
    return moment(date, format).format(format) === date;
}
