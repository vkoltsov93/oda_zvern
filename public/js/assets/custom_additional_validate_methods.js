$.validator.addMethod(
    "notNone",
    function (value, element, option) {
        return value != 'none';
    },
);
$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var check = false;
        return this.optional(element) || regexp.test(value);
    },
);
$.validator.addMethod(
    'minDate',
    function (value, element, option){
        let diffDays = diffDateDays(value, 'DD.MM.YYYY');
        if (diffDays > 0) return  true;
        return diffDays >= -10 && diffDays <= 0;
    },
);
$.validator.addMethod(
    'maxDate',
    function (value, element, option){
        let diffDays = diffDatePartDays(value, 'DD.MM.YYYY');
        return diffDays <= 0;
    },
);
