let requiredMessage = "Обов'язково до заповнення";
let regexString = /^[а-яА-ЯїієґҐЇІЄ']+$/;
let regexStringMessage = "Тільки українські літери";
let minLengthMessage = "Не меньше 2х символів";
let formRules = {
    onchange: true,
    onkeypress: true,
    ignore: [],
    rules: {
        kind: {
            notNone: true,
        },
        type: {
            notNone: true,
        },
        admission_type: {
            notNone: true,
        },
        sign: {
            notNone: true,
        },
        subject: {
            notNone: true,
        },
        personCount: {
            min: {
                param: 2,
                depends: function (){
                    if($('#subject').val() == 2){
                        return true;
                    }
                }
            },
            required: true,
        },
        incomingDate: {
            required: true,
            minDate: true,
            maxDate: true,
        },
        last_name: {
            required: {
                depends: function (){
                    return $('#subject').val() != 3;
                },
            },
            regex: regexString,
            minlength: 2,
        },
        first_name: {
            required: {
                depends: function (){
                    return $('#subject').val() != 3;
                },
            },
            regex: regexString,
            minlength: 2,
        },
        middle_name: {
            regex: regexString,
            minlength: 2,
        },
        gender: {
            notNone: {
                depends: function (){
                    return $('#subject').val() != 3;
                },
            },
        },
        district: {
            notNone: {
                depends: function (){
                    return $('#local_checkbox').is(':checked');
                },
            },
        },
        otg: {
            notNone: {
                depends: function (){
                    return $('#local_checkbox').is(':checked');
                },
            },
        },
        locality: {
            notNone: {
                depends: function (){
                    return $('#local_checkbox').is(':checked');
                },
            },
        },
        address: {
            required: true,
        },
        region_other: {
            required: {
                depends: function (){
                    return !$('#local_checkbox').is(':checked');
                },
            },
            minlength: 2,
        },
        district_other: {
            required: {
                depends: function (){
                    return !$('#local_checkbox').is(':checked');
                },
            },
            minlength: 2,
        },
        locality_other: {
            required: {
                depends: function (){
                    return !$('#local_checkbox').is(':checked');
                },
            },
            minlength: 2,
        },
        zip: {
            digits: true,
            minlength: 5,
            maxlength: 5,
        },
        email: {
            minlength: 2,
            regex: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
        },
        social_status: {
            notNone: true,
        },
        category: {
            notNone: true,
        },
        from: {
            notNone: true,
        },
        orgDate: {
            required: {
                depends: function (){
                  return  $('#from').val() != 1;
                },
            },
        },
        orgIndex:{
            required: {
                depends: function (){
                    return  $('#from').val() != 1;
                },
            },
        },
        appeal_text: {
            required: true,
            minlength: 2,
        },
        "doers[]": {
            notNone: true,
        },
        "questions[]": {
            notNone: true,
        },
        "sub_questions[]": {
            notNone: true,
        },
        responsible: {
            notNone: true,
        },
        resolution_text: {
            required: true,
            minlength: 2,
        }


    },
    messages: {
        kind: {
            notNone: requiredMessage,
        },
        type: {
            notNone: requiredMessage,
        },
        admission_type: {
            notNone: requiredMessage,
        },
        sign: {
            notNone: requiredMessage,
        },
        subject: {
            notNone: requiredMessage,
        },
        personCount: {
            min: "Кількість не може бути меньше 2х",
            required: requiredMessage,
        },
        incomingDate: {
            required: requiredMessage,
            minDate: "Дата не може бути меньше 10 днів",
            maxDate: "Дата не може бути майбутньою",
        },
        last_name: {
            required: requiredMessage,
            regex: regexStringMessage,
            minlength: minLengthMessage,
        },
        first_name: {
            required: requiredMessage,
            regex: regexStringMessage,
            minlength: minLengthMessage,
        },
        middle_name: {
            regex: regexStringMessage,
            minlength: minLengthMessage,
        },
        gender: {
            notNone: requiredMessage,
        },
        district: {
            notNone: requiredMessage,
        },
        otg: {
            notNone: requiredMessage,
        },
        locality: {
            notNone: requiredMessage,
        },
        address: {
            required: requiredMessage,
        },
        region_other: {
            required: requiredMessage,
            minlength: minLengthMessage,
        },
        district_other: {
            required: requiredMessage,
            minlength: minLengthMessage,
        },
        locality_other: {
            required: requiredMessage,
            minlength: minLengthMessage,
        },
        zip: {
            digits: "Тільки цифри",
            minlength: "Індекс має містити 5 цифр",
            maxlength: "Індекс має містити 5 цифр",
        },
        email: {
            regex: "Невірний формат ел. пошти",
            minlength: "Невірний формат ел. пошти",
        },
        social_status: {
            notNone: requiredMessage,
        },
        category: {
            notNone: requiredMessage,
        },
        from: {
            notNone: requiredMessage,
        },
        orgDate: {
            required: requiredMessage,
        },
        orgIndex:{
            required: requiredMessage,
        },
        appeal_text: {
            required: requiredMessage,
            minlength: minLengthMessage,
        },
        "doers[]": {
            notNone: requiredMessage,
        },
        "questions[]": {
            notNone: requiredMessage,
        },
        "sub_questions[]": {
            notNone: requiredMessage,
        },
        responsible: {
            notNone: requiredMessage,
        },
        resolution_text: {
            required: requiredMessage,
            minlength: minLengthMessage,
        }

    },

    errorClass: "error-input",
    errorPlacement: function (error, element) {
        let elementId = $(element).attr('id');
        $(element).addClass('error-input').removeClass('success-input');
        $('.' + elementId + "_error-message").text(error.text()).show();
    },
    success: function (error, element) {
        let elementId = $(element).attr('id');
        $(element).removeClass('error-input').addClass('success-input');
        $('.' + elementId + "_error-message").hide();
    },
    submitHandler: function (form){
        if($('#local_checkbox').is(':checked')){
            $('#country').attr('disabled', true);
            $('#region_other').attr('disabled', true);
            $('#district_other').attr('disabled', true);
            $('#locality_other').attr('disabled', true);
        }else {
            $('#district').attr('disabled', true);
            $('#otg').attr('disabled', true);
            $('#locality').attr('disabled', true);
        }
        if($('#subject').val() == 3){
            console.log(111);
            $('#gender').attr('disabled', true);
        }
        //console.log($(form).serializeArray());
        form.submit();
    },

}
