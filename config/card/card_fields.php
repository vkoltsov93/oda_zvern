<?php
return [
    'kind_id' => [
        'form' => 'kind',
    ],
    'type_id' => [
        'form' => 'type',
    ],
    'admission_type_id' => [
        'form' => 'admission_type',
    ],
    'sign_id' => [
        'form' => 'sign',
    ],
    'sign_double' => [
        'form' => 'sign_double',
    ],
    'subject_id' => [
        'form' => 'subject',
    ],
    'person_count' => [
        'form' => 'personCount',
    ],
    'incoming_date' => [
        'form' => 'incomingDate',
    ],
    'last_name' => [
        'form' => 'last_name',
    ],
    'first_name' => [
        'form' => 'first_name',
    ],
    'middle_name' => [
        'form' => 'middle_name',
    ],
    'district_id' => [
        'form' => 'district',
    ],
    'union_community_id' => [
        'form' => 'union_community',
    ],
    'locality_id' => [
        'form' => 'locality',
    ],
    'gender' => [
        'form' => 'gender',
    ],
    'region' => [
        'form' => 'region_other',
    ],
    'district' => [
        'form' => 'district_other',
    ],
    'locality' => [
        'form' => 'locality_other',
    ],
    'address' => [
        'form' => 'address',
    ],
    'zip' => [
        'form' => 'zip',
    ],
    'phone' => [
        'form' => 'phone',
    ],
    'phone_extra' => [
        'form' => 'phone_extra',
    ],
    'email' => [
        'form' => 'email',
    ],
    'category_id' => [
        'form' => 'category',
    ],
    'social_status_id' => [
        'form' => 'social_status',
    ],
    'corr_note' => [
        'form' => 'corrNote',
    ],
    'admission_id' => [
        'form' => 'admission',
    ],
    'org_date' => [
        'form' => 'orgDate',
    ],
    'org_note' => [
        'form' => 'orgNote',
    ],
    'org_index' => [
        'form' => 'orgIndex',
    ],
    'question_id' => [
        'is_array' => true,
        'form' => 'questions',
    ],
    'subquestion_id' => [
        'is_array' => true,
        'form' => 'sub_questions',
    ],
    'appeal_text_note' => [
        'form' => 'appeal_text',
    ],
    'doer_id' => [
        'is_array' => true,
        'form' => 'doers',
    ],
    'responsible_id' => [
        'form' => 'responsible',
    ],
    'term' => [
        'form' => 'term_date',
    ],
    'is_local' => [
        'form' => 'local_checkbox',
    ],
    'resolution' => [
        'form' => 'resolution_text',
    ],
    'region_id' => [
        'form' => 'region'
    ],

];
