<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('gender')->nullable();
            $table->foreignId('district_id')->nullable();
            $table->foreignId('union_community_id')->nullable();
            $table->foreignId('locality_id')->nullable();
            $table->string('address')->nullable();
            $table->string('zip')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone_extra')->nullable();
            $table->string('email')->nullable();
            $table->foreignId('social_status_id');
            $table->foreignId('category_id');
            $table->string('corr_note')->nullable();
            $table->foreignId('sign_id');
            $table->foreignId('type_id');
            $table->foreignId('admission_type_id');
            $table->foreignId('kind_id');
            $table->foreignId('subject_id');
            $table->unsignedSmallInteger('person_count')->default(1);
            $table->date('incoming_date');
            $table->boolean('is_local')->default(1);
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('district')->nullable();
            $table->string('locality')->nullable();
            $table->foreignId('admission_id');
            $table->date('org_date')->nullable();
            $table->string('org_index')->nullable();
            $table->string('org_note')->nullable();
            $table->longText('appeal_text_note');
            $table->foreignId('responsible_id');
            $table->unsignedBigInteger('responsible_id_close')->nullable();
            $table->date('term')->nullable();
            $table->longText('resolution');
            $table->boolean('is_on_control')->default(0);
            $table->boolean('user_id_set_on_control')->nullable();
            $table->boolean('is_closed')->default(0);
            $table->boolean('user_id');
            $table->boolean('user_id_closed')->nullable();
            $table->string('closed_number')->nullable();
            $table->date('closed_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->dropIfExists();
        });
    }
}
