<?php

namespace Database\Seeders;

use App\Models\ResponsiblePosition;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class PositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('positions');


        foreach ($data as $item){
            $responsible = ResponsiblePosition::where([
                'title' => $item['title'],
            ])->first();
            if (is_null($responsible)){
                ResponsiblePosition::create([
                    'title' => $item['title'],
                ]);
            }
        }
    }
}
