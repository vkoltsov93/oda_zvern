<?php

namespace Database\Seeders;

use App\Models\UnionCommunity;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class UnionCommunitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('otgs');

        foreach ($data as $item){
            $responsible = UnionCommunity::where([
                'name' => $item['name'],
                'district_id' => $item['region_id'],
            ])->first();
            if (is_null($responsible)){
                UnionCommunity::create([
                    'name' => $item['name'],
                    'district_id' => $item['region_id'],
                ]);
            }
        }
    }
}
