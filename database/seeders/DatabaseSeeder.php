<?php

namespace Database\Seeders;

use App\Models\Result;
use App\Models\Subject;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            QuestionsSeeder::class,
            SubQuestionsSeeder::class,
            ResponsiblesSeeder::class,
            DoersSeeder::class,
            ResolutionsSeeder::class,
            PositionsSeeder::class,
            DistrictsSeeder::class,
            UnionCommunitiesSeeder::class,
            LocalitiesSeeder::class,
            SignsSeeder::class,
            TypesSeeder::class,
            AdmissionTypesSeeder::class,
            KindsSeeder::class,
            SubjectsSeeder::class,
            SocialStatusesSeeder::class,
            CategoriesSeeder::class,
            AdmissionsSeeder::class,
//            CardsSeeder::class,
//            CardQuestionPivotSeeder::class,
//            CardDoerPivotSeeder::class,
            ResultsSeeder::class,
        ]);
    }
}
