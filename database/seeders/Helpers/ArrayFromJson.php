<?php

namespace Database\Seeders\Helpers;

class ArrayFromJson
{
    public static function get($filename){

        $paths = [
            'admissions' => 'admissions.json',
            'anonymousclients' => 'anonymousclients.json',
            'cardquestion' => 'cardquestion.json',
            'cards' => 'cards.json',
            'categories' => 'categories.json',
            'clients' => 'clients.json',
            'districts' => 'districts.json',
            'otgs' => 'otgs.json',
            'otheranonymousclients' => 'otheranonymousclients.json',
            'otherclients' => 'otherclients.json',
            'positions' => 'positions.json',
            'resolutions' => 'resolutions.json',
            'responsibles' => 'responsibles.json',
            'socialStatuses' => 'socialStatuses.json',
            'subquestions' => 'subquestions.json',
            'doers' => 'doers.json',
            'localities' => 'localities.json',
            'cardresults' => 'cardresults.json',
            'carddoers' => 'carddoers.json',
            'results' => 'results.json',
        ];

        $jsonFile = file_get_contents(base_path().'/jsons_old_db/'.$paths[$filename]);
        $data = json_decode($jsonFile, true);
        return $data;
    }
}
