<?php

namespace Database\Seeders;

use App\Models\Responsible;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class ResponsiblesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('responsibles');
        $dataPosition = ArrayFromJson::get('positions');


        foreach ($data as $item){
            foreach ($dataPosition as $itemPosition){
                if ($item['position_id'] == $itemPosition['id']){
                    $responsible = Responsible::where([
                        'name' => $item['title'],
                    ])->first();
                    if (is_null($responsible)){
                        Responsible::create([
                            'name' => $item['title'],
                            'responsible_position_id' => $itemPosition['id'],
                        ]);
                    }
                }
            }
        }
    }
}
