<?php

namespace Database\Seeders;

use App\Models\Kind;
use Illuminate\Database\Seeder;

class KindsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['title' => 'Заява'],
            ['title' => 'Скарга'],
            ['title' => 'Пропозиція'],
        ];

        foreach ($data as $item){
            $sign = Kind::where('title', $item['title'])->first();
            if (is_null($sign)){
                Kind::create($item);
            }
        }
    }
}
