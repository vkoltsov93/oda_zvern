<?php

namespace Database\Seeders;

use App\Models\SocialStatus;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class SocialStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('socialStatuses');

        $order_index = 1;

        foreach ($data as $item){
            $doer = SocialStatus::where([
                'title' => $item['title'],
            ])->first();
            if (is_null($doer)){
                SocialStatus::create([
                    'title' => $item['title'],
                    'order_index' => $order_index,
                ]);
                $order_index++;
            }
        }
    }
}
