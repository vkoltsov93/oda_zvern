<?php

namespace Database\Seeders;

use App\Models\Doer;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class DoersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('doers');

        $i = 1;
        foreach ($data as $item){
            $doer = new Doer();
            $doer->name = $item['title'];
            $doer->organization = $item['organization'];
            $doer->organization_to = $item['organizationd'];
            $doer->email = $item['email'];
            $doer->order_index = $i;
            $doer->created_at = $item['created_at'];
            $doer->updated_at = $item['updated_at'];
            $doer->deleted_at = strlen($item['deleted_at']) == 0 ? null : $item['deleted_at'];
            $doer->save();
            $i++;
        }
    }
}
