<?php

namespace Database\Seeders;

use App\Models\Sign;
use Illuminate\Database\Seeder;

class SignsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['title' => 'Первинне'],
            ['title' => 'Повторне'],
            ['title' => 'Дублетне'],
        ];

        foreach ($data as $item){
            $sign = Sign::where('title', $item['title'])->first();
            if (is_null($sign)){
                Sign::create($item);
            }
        }
    }
}
