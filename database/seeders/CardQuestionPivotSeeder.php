<?php

namespace Database\Seeders;

use App\Models\Card;
use App\Models\SubQuestion;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CardQuestionPivotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('cardquestion');
        $cardData = ArrayFromJson::get('cards');
        $subquestionsData = ArrayFromJson::get('subquestions');

        DB::table('card_question')->truncate();

        foreach ($data as $item) {
            foreach ($cardData as $cardArray) {
                if ($cardArray['id'] == $item['card_id']) {
                    foreach ($subquestionsData as $subArray) {
                        if ($subArray['id'] == $item['subquestion_id']) {

                            $card = Card::where('number', $cardArray['number'])->first();
                            if (!(is_null($card))) {
                                $subquestion = SubQuestion::where('index', $subArray['index'])
                                    ->with('question')
                                    ->first();
                                $card->questions()->attach($subquestion->question, ['sub_question_id' => $subquestion->id]);
                            }

                        }
                    }


                }
            }


        }
    }
}
