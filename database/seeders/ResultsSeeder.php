<?php

namespace Database\Seeders;

use App\Models\Result;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class ResultsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('results');

        $i = 1;
        foreach ($data as $item){
            Result::create([
                'title' => $item['title'],
                'order_index' => $i,
            ]);
            $i++;
        }
    }
}
