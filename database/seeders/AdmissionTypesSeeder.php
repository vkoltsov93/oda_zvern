<?php

namespace Database\Seeders;

use App\Models\AdmissionType;
use Illuminate\Database\Seeder;

class AdmissionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['title' => 'Поштою'],
            ['title' => 'На особистому прийомі'],
            ['title' => 'Через органи влади'],
            ['title' => 'Через засоби масової інформації'],
            ['title' => 'Від інших органів, установ, організацій'],
            ['title' => 'Через уповноважену особу'],
        ];

        foreach ($data as $item){
            $sign = AdmissionType::where('title', $item['title'])->first();
            if (is_null($sign)){
                AdmissionType::create($item);
            }
        }
    }
}
