<?php

namespace Database\Seeders;

use App\Models\District;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class DistrictsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('districts');

        foreach ($data as $item){
            $responsible = District::where([
                'name' => $item['name'],
                'name_for' => $item['namez'],
            ])->first();
            if (is_null($responsible)){
                District::create([
                    'name' => $item['name'],
                    'name_for' => $item['namez'],
                ]);
            }
        }
    }
}
