<?php

namespace Database\Seeders;

use App\Models\Resolution;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class ResolutionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('resolutions');

        foreach ($data as $item){
            $resolution = Resolution::where([
                'content' => $item['title'],
            ])->first();
            if (is_null($resolution)){
                Resolution::create([
                    'content' => $item['title'],
                ]);
            }
        }
    }
}
