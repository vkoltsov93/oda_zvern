<?php

namespace Database\Seeders;

use App\Models\Admission;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class AdmissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('admissions');

        $order_index = 1;

        foreach ($data as $item){
            $doer = Admission::where([
                'title' => $item['title'],
            ])->first();
            if (is_null($doer)){
                Admission::create([
                    'title' => $item['title'],
                    'order_index' => $order_index,
                ]);
                $order_index++;
            }
        }
    }
}
