<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['title' => 'Індивідуальне'],
            ['title' => 'Колективне'],
            ['title' => 'Анонімне'],
        ];

        foreach ($data as $item){
            $sign = Subject::where('title', $item['title'])->first();
            if (is_null($sign)){
                Subject::create($item);
            }
        }
    }
}
