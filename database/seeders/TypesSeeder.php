<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['title' => 'Лист'],
            ['title' => 'Усне'],
            ['title' => 'Телеграма'],
        ];

        foreach ($data as $item){
            $sign = Type::where('title', $item['title'])->first();
            if (is_null($sign)){
                Type::create($item);
            }
        }
    }
}
