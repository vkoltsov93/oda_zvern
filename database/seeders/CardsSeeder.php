<?php

namespace Database\Seeders;

use App\Models\Card;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class CardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('cards');

        $dataClients = ArrayFromJson::get('clients');

        $dataOtherClients = ArrayFromJson::get('otherclients');

        $dataOtherAnonymousClients = ArrayFromJson::get('otheranonymousclients');

        $dataAnonymousClients = ArrayFromJson::get('anonymousclients');




        foreach ($data as $item){
            $tempArray = [];
            $tempArray['number'] = $item['number'];
            $tempArray['social_status_id'] = $item['status_id'];
            $tempArray['category_id'] = $item['category_id'];
            $tempArray['corr_note'] = strlen($item['corrNote']) == 0 ? null : $item['corrNote'];
            $tempArray['sign_id'] = $item['sign_id'];
            $tempArray['type_id'] = $item['type_id'];
            $tempArray['admission_type_id'] = $item['receipt_id'];
            $tempArray['kind_id'] = $item['kind_id'];
            $tempArray['subject_id'] = $item['subject_id'];
            $tempArray['person_count'] = strlen($item['personCount']) == 0 ? 1 : ($item['personCount'] < 0 ? $item['personCount'] * -1 : $item['personCount']);
            $tempArray['incoming_date'] = strlen($item['incomingDate']) == 0 ? null: $item['incomingDate'];
            $tempArray['admission_id'] = $item['from_id'];
            $tempArray['org_date'] = strlen($item['orgDate']) == 0 ? null : $item['orgDate'];
            $tempArray['org_index'] = strlen($item['orgIndex']) == 0 ? null : $item['orgIndex'];
            $tempArray['appeal_text_note'] = strlen($item['appealTextNote']) == 0 ? null : $item['appealTextNote'];
            $tempArray['responsible_id'] = strlen($item['responsible_id']) == 0 ? null : $item['responsible_id'];
            $tempArray['responsible_id_close'] = strlen($item['responsible_id_close']) == 0 ? null : $item['responsible_id_close'];
            $tempArray['term'] = strlen($item['term']) == 0 ? null : $item['term'];
            $tempArray['resolution'] = strlen($item['resolution']) == 0 ? null : $item['resolution'];
            $tempArray['is_on_control'] = strlen($item['isOnControl']) == 0 ? null : $item['isOnControl'];
            $tempArray['user_id_set_on_control'] = strlen($item['user_id_onControl']) == 0 ? null : $item['user_id_onControl'];
            $tempArray['user_id_closed'] = strlen($item['user_id_close']) == 0 ? null : $item['user_id_close'];
            $tempArray['user_id'] = strlen($item['user_id']) == 0 ? null : $item['user_id'];
            $tempArray['created_at'] = $item['created_at'];
            $tempArray['updated_at'] = $item['updated_at'];
            $tempArray['is_closed'] = is_null($item['responsible_id_close']) ? null : 1;

            if ($item['other_client_id'] > 0){
                foreach ($dataOtherClients as $client)
                {
                    if($item['other_client_id'] == $client['id']){
                        $tempArray['is_local'] = false;
                        $tempArray['country'] = 1;
                        $tempArray['first_name'] = strlen($client['firstName']) == 0 ? null : $client['firstName'];
                        $tempArray['middle_name'] = strlen($client['middleName']) == 0 ? null : $client['middleName'];
                        $tempArray['last_name'] = strlen($client['lastName']) == 0 ? null : $client['lastName'];
                        $tempArray['gender'] = $client['gender'] == 'm' ? 1 : 0;
                        $tempArray['region'] = strlen($client['other-region']) == 0 ? null : $client['other-region'];
                        $tempArray['district'] = strlen($client['other-district']) == 0 ? null : $client['other-district'];
                        $tempArray['locality'] = strlen($client['other-locality']) == 0 ? null : $client['other-locality'];
                        $tempArray['address'] = strlen($client['other-address']) == 0 ? null: $client['other-address'];
                        $tempArray['zip'] = strlen($client['index']) == 0 ? null : $client['index'];
                        $tempArray['phone'] = strlen($client['phone']) == 0 ? null : $client['phone'];
                        $tempArray['email'] = strlen($client['email']) == 0 ? null : $client['email'];
                    }
                }
            }
            if ($item['other_anonymous_id'] > 0){
                foreach ($dataOtherAnonymousClients as $client){
                    if ($item['other_anonymous_id'] == $client['id']){
                        $tempArray['is_local'] = false;
                        $tempArray['country'] = 2;
                        $tempArray['region'] = strlen($client['other-region']) == 0 ? null : $client['other-region'];
                        $tempArray['district'] = strlen($client['other-district']) == 0 ? null : $client['other-district'];
                        $tempArray['locality'] = strlen($client['other-locality']) == 0 ? null : $client['other-locality'];
                        $tempArray['address'] = strlen($client['other-address']) == 0 ? null: $client['other-address'];
                        $tempArray['zip'] = strlen($client['index']) == 0 ? null : $client['index'];
                        $tempArray['phone'] = strlen($client['phone']) == 0 ? null : $client['phone'];
                        $tempArray['email'] = strlen($client['email']) == 0 ? null : $client['email'];
                    }
                }
            }
            if ($item['anonymous_appeal_id'] > 0){
                foreach ($dataAnonymousClients as $client){
                    $tempArray['is_local'] = true;
                    $tempArray['country'] = 1;
                    $tempArray['district_id'] = $client['region_id'];
                    $tempArray['union_community_id'] = $client['otg_id'];
                    $tempArray['locality_id'] = $client['locality_id'];
                    $tempArray['address'] = strlen($client['address']) == 0 ? null: $client['address'];
                    $tempArray['zip'] = strlen($client['index']) == 0 ? null : $client['index'];
                    $tempArray['phone'] = strlen($client['phone']) == 0 ? null : $client['phone'];
                    $tempArray['email'] = strlen($client['email']) == 0 ? null : $client['email'];
                }
            }
            if ($item['client_id'] > 0){
                foreach ($dataClients as $client){
                    if ($item['client_id'] == $client['id']){
                        $tempArray['is_local'] = true;
                        $tempArray['country'] = 1;
                        $tempArray['first_name'] = strlen($client['firstName']) == 0 ? null : $client['firstName'];
                        $tempArray['middle_name'] = strlen($client['middleName']) == 0 ? null : $client['middleName'];
                        $tempArray['last_name'] = strlen($client['lastName']) == 0 ? null : $client['lastName'];
                        $tempArray['gender'] = $client['gender'] == 'm' ? 1 : 0;
                        $tempArray['district_id'] = $client['region_id'];
                        $tempArray['union_community_id'] = $client['otg_id'];
                        $tempArray['locality_id'] = $client['locality_id'];
                        $tempArray['address'] = strlen($client['address']) == 0 ? null: $client['address'];
                        $tempArray['zip'] = strlen($client['index']) == 0 ? null : $client['index'];
                        $tempArray['phone'] = strlen($client['phone']) == 0 ? null : $client['phone'];
                        $tempArray['email'] = strlen($client['email']) == 0 ? null : $client['email'];

                    }
                }
            }
            Card::create($tempArray);

        }
    }
}
