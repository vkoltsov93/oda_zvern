<?php

namespace Database\Seeders;

use App\Models\Locality;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class LocalitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('localities');



        foreach ($data as $item){
            $responsible = Locality::where([
                'name' => $item['name'],
                'name_for' => $item['namez'],
                'union_community_id' => $item['otg_id'],
            ])->first();
            if (is_null($responsible)){
                Locality::create([
                    'name' => $item['name'],
                    'name_for' => $item['namez'],
                    'union_community_id' => $item['otg_id'],
                ]);
            }
        }
    }
}
