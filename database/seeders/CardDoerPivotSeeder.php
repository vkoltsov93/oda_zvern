<?php

namespace Database\Seeders;

use App\Models\Card;
use App\Models\Doer;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class CardDoerPivotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cardDoers = ArrayFromJson::get('carddoers');
        $cards = ArrayFromJson::get('cards');
        $doers = ArrayFromJson::get('doers');

        foreach ($cardDoers as $cardDoer) {
            foreach ($cards as $card) {
                if ($cardDoer['card_id'] == $card['id']) {
                    foreach ($doers as $doer) {
                        if ($doer['id'] == $cardDoer['doer_id']) {

                            $cardTemp = Card::where('number', $card['number'])->first();
                            if (!is_null($cardTemp)) {
                                $doerTemp = Doer::where('organization', $doer['organization'])->first();
                                $cardTemp->doers()->attach($doerTemp->id);
                            }
                        }
                    }
                }


            }
        }

    }
}
