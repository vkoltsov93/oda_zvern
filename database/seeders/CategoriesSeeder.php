<?php

namespace Database\Seeders;

use App\Models\Category;
use Database\Seeders\Helpers\ArrayFromJson;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ArrayFromJson::get('categories');
        $order_index = 1;

        foreach ($data as $item){
            $doer = Category::where([
                'title' => $item['title'],
            ])->first();
            if (is_null($doer)){
                Category::create([
                    'title' => $item['title'],
                    'order_index' => $order_index,
                ]);
                $order_index++;
            }
        }
    }
}
