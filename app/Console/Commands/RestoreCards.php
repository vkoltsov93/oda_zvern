<?php

namespace App\Console\Commands;

use App\Models\Card;
use App\Models\Doer;
use App\Models\Question;
use App\Models\Responsible;
use App\Models\SubQuestion;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RestoreCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restore:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $jsonFile = file_get_contents(base_path().'/jsons_old_db/cardresults.json');
        $dataCardResults = json_decode($jsonFile, true);

        $jsonFile = file_get_contents(base_path().'/jsons_old_db/cards.json');
        $dataCards = json_decode($jsonFile, true);

        $jsonFile = file_get_contents(base_path().'/jsons_old_db/doers.json');
        $dataDoers = json_decode($jsonFile, true);

        $jsonFile = file_get_contents(base_path().'/jsons_old_db/subquestions.json');
        $dataSubquestions = json_decode($jsonFile, true);

        $jsonFile = file_get_contents(base_path().'/jsons_old_db/results.json');
        $dataResults = json_decode($jsonFile, true);


        $cardQuestions = DB::table('card_question')
            ->select(
                [
                    'card_question.card_id',
                    'card_question.question_id',
                    'card_question.sub_question_id',
                    'cards.number',
                    'doers.id',
                    'doers.name',
                    'doers.organization',
                ])
            ->join('cards', 'cards.id', '=', 'card_question.card_id')
            ->join('card_doer', 'card_doer.card_id', '=', 'cards.id')
            ->join('doers', 'doers.id', '=', 'card_doer.doer_id')
            ->get();

        $i = 1;
        foreach ($dataCards as $dataCard){
            $i++;
            foreach ($cardQuestions as $cardQuestion){
                if ($cardQuestion->number == $dataCard['number']){
                    foreach ($dataCardResults as $cardResult){
                        if ($cardResult['result_id'] == 0){
                            continue;
                        }
                        if ($cardResult['card_id'] == $dataCard['id']){


                            $subQuestionIndex = null;
                            foreach ($dataSubquestions as $dataSubquestion){
                                if ($dataSubquestion['id'] == $cardResult['subquestion_id']){
                                    $subQuestionIndex = $dataSubquestion['index'];
                                }
                            }
                            $doerDataArray = [];
                            foreach ($dataDoers as $dataDoer){
                                if ($dataDoer['id'] == $cardResult['doer_id']){
                                    $doerDataArray['title'] = $dataDoer['title'];
                                    $doerDataArray['organization'] = $dataDoer['organization'];
                                }
                            }
                            $subQuestion = SubQuestion::where('index', $subQuestionIndex)->first();
                            if (is_null($subQuestion)){
                                $question = null;
                            }else{
                                $question = $subQuestion->question;
                            }
                            dump($cardResult['result_id']);

                            $card = Card::where('number', $dataCard['number'])->first();

                            $attacheData = [
                                'question_id' => $question->id ?? null,
                                'sub_question_id' => $subQuestion->id ?? null,
                            ];

                            if (!(empty($doerDataArray))){
                                $doer = Doer::where(['name' => $doerDataArray['title'], 'organization' => $doerDataArray['organization']])->first();
                                $attacheData['doer_id'] = $doer->id;
                            }
                            else{
                                $attacheData['doer_id'] = $card->doers[0]->id;
                            }

                            $responsible = Responsible::where('name', $dataCard['responsible_id_close'])->first();
                            $card->responsible_id_close = $responsible->id ?? $card->responsible_id;
                            $card->save();

                            $card->results()->attach($cardResult['result_id'], $attacheData);
                        }
                    }
                }
            }
        }
    }
}
