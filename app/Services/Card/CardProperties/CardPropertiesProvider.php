<?php

namespace App\Services\Card\CardProperties;

use Carbon\Laravel\ServiceProvider;

class CardPropertiesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CardPropertiesService::class, function(){
            return new CardProperties();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
