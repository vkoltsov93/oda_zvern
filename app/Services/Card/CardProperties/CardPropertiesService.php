<?php

namespace App\Services\Card\CardProperties;

use Illuminate\Database\Eloquent\Collection;

interface CardPropertiesService
{
    public const CACHED_TIME = 3600 * 24;

    public function getAll(): array;

    public function getQuestions(): Collection;

    public function getSubQuestions(): Collection;

    public function getDoers(): Collection;

    public function getResponsibles(): Collection;

    public function getResolutions(): Collection;

    public function getDistrics(): Collection;

    public function getUnionCommunities(): Collection;

    public function getLocalities(): Collection;
}
