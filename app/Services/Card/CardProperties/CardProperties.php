<?php

namespace App\Services\Card\CardProperties;

use App\Services\Card\CardProperties\Actions\CachedProperties;
use App\Services\Card\CardProperties\Actions\PropertiesList;
use Illuminate\Database\Eloquent\Collection;

class CardProperties implements CardPropertiesService
{

    public function getAll(): array
    {
        return PropertiesList::run($this);
    }

    public function getQuestions(): Collection
    {
        return CachedProperties::getQuestions();
    }

    public function getSubQuestions(): Collection
    {
        return CachedProperties::getSubQuestions();
    }

    public function getDoers(): Collection
    {
        return CachedProperties::getDoers();
    }

    public function getResponsibles(): Collection
    {
        return CachedProperties::getResponsibles();
    }

    public function getResolutions(): Collection
    {
        return CachedProperties::getResolutions();
    }

    public function getDistrics(): Collection
    {
        return CachedProperties::getDistricts();
    }

    public function getUnionCommunities(): Collection
    {
        return CachedProperties::getUnionCommunities();
    }

    public function getLocalities(): Collection
    {
        return CachedProperties::getLocalities();
    }
}
