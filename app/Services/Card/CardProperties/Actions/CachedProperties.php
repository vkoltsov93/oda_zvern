<?php

namespace App\Services\Card\CardProperties\Actions;

use App\Models\District;
use App\Models\Doer;
use App\Models\Locality;
use App\Models\Question;
use App\Models\Resolution;
use App\Models\Responsible;
use App\Models\SubQuestion;
use App\Models\UnionCommunity;
use App\Services\Card\CardProperties\CardPropertiesService;
use Illuminate\Support\Facades\Cache;

class CachedProperties
{
    public static function getQuestions(){
        return Cache::remember('questions', CardPropertiesService::CACHED_TIME, function () {
            return Question::select(['id', 'title', 'index'])->get();
        });
    }

    public static function getSubQuestions(){
        return Cache::remember('subquestions', CardPropertiesService::CACHED_TIME, function () {
            return SubQuestion::select(['id', 'title', 'index', 'question_id'])->with('question')->get();
        });
    }

    public static function getDoers(){
        return Cache::remember('doers', CardPropertiesService::CACHED_TIME, function () {
            return Doer::select(['id', 'name', 'organization', 'organization_to', 'email', 'order_index'])->get();
        });
    }

    public static function getResponsibles(){
        return Cache::remember('responsibles', CardPropertiesService::CACHED_TIME, function () {
            return Responsible::select(['id', 'name', 'is_active', 'responsible_position_id'])->with('responsiblePosition')->get();
        });
    }
    public static function getResolutions(){
        return Cache::remember('resolutions', CardPropertiesService::CACHED_TIME, function () {
            return Resolution::select(['id', 'content'])->get();
        });
    }
    public static function getDistricts(){
        return Cache::remember('districts', CardPropertiesService::CACHED_TIME, function () {
            return District::select(['id', 'name', 'name_for'])->get();
        });
    }
    public static function getUnionCommunities(){
        return Cache::remember('union_communities', CardPropertiesService::CACHED_TIME, function () {
            return UnionCommunity::select(['id', 'name', 'district_id'])->get();
        });
    }
    public static function getLocalities(){
        return Cache::remember('localities', CardPropertiesService::CACHED_TIME, function () {
            return Locality::select(['id', 'name', 'name_for', 'union_community_id'])->get();
        });
    }
}
