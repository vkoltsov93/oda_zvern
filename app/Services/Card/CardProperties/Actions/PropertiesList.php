<?php

namespace App\Services\Card\CardProperties\Actions;

use App\Models\Question;
use App\Models\Responsible;
use App\Models\SubQuestion;
use App\Services\Card\CardProperties\CardProperties;
use Illuminate\Support\Facades\Cache;

class PropertiesList
{
    public static function run(CardProperties $cardProperties)
    {
        $kinds = config('card.properties.kinds');
        $types = config('card.properties.types');
        $admissionTypes = config('card.properties.admission_types');
        $signs = config('card.properties.signs');
        $subjects = config('card.properties.subjects');
        $genders = config('card.properties.genders');
        $socialStatuses = config('card.properties.social_statuses');
        $admissions = config('card.properties.admissions');
        $countries = config('card.properties.countries');
        $categories = config('card.properties.categories');

        $localities = $cardProperties->getLocalities();
        $districts = $cardProperties->getDistrics();
        $questions = $cardProperties->getQuestions();
        $subquestions = $cardProperties->getSubQuestions();
        $responsibles = $cardProperties->getResponsibles();
        $doers = $cardProperties->getDoers();
        $resolutions = $cardProperties->getResolutions();

        return [
            'kinds' => $kinds,
            'types' => $types,
            'admission_types' => $admissionTypes,
            'signs' => $signs,
            'subjects' => $subjects,
            'genders' => $genders,
            'social_statuses' => $socialStatuses,
            'admissions' => $admissions,
            'countries' => $countries,
            'categories' => $categories,
            'districts' => $districts,
            'questions' => $questions,
            'subquestions' => $subquestions,
            'responsibles' => $responsibles,
            'doers' => $doers,
            'resolutions' => $resolutions,
        ];
    }
}
