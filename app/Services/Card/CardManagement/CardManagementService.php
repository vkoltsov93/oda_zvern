<?php

namespace App\Services\Card\CardManagement;

interface CardManagementService
{
    public function store(array $data): bool;

    public function update(array $data): bool;
}
