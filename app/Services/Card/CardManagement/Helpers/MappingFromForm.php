<?php

namespace App\Services\Card\CardManagement\Helpers;

use Carbon\Carbon;

class MappingFromForm
{
    public static function handle(array $data){
        $fields = config('card.card_fields');
        $mappedData = [];
        $datesArray = ['term_date', 'orgDate', 'incomingDate'];

        foreach ($data as $key => $value){
            if (in_array($key, $datesArray)){
                $data[$key] = Carbon::parse($value)->format('Y-m-d H:i:s');
            }
        }

        foreach ($fields as $key => $value){

            if (isset($data[$value['form']])){
                $mappedData[$key] = $data[$value['form']];
            }
            else{
                continue;
            }
        }
        if(isset($mappedData['is_local'])){
            $mappedData['is_local'] = $mappedData['is_local'] == 'on' ? 1 : 0;
        }
        return $mappedData;
    }
}
