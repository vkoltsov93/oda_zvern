<?php

namespace App\Services\Card\CardManagement\Helpers;

use App\Models\Card;
use App\Models\Constants\Card\CardAdmission;
use App\Models\Constants\Card\CardAdmissionType;
use App\Models\Constants\Card\CardSign;
use App\Models\Constants\Card\CardSubject;
use Illuminate\Support\Facades\DB;

class GenerateNumber
{
    public static function handle(array $data){
        return self::getNumber($data);
    }
    public static function getNumber(array $data): string
    {
        if ($data['sign_id'] == CardSign::DOUBLE){
            return self::getNumberForDouble($data['sign_double']);
        }

        $number =  self::getNextId($data);
        $number = $number.'/10';

        if (!in_array($data['subject_id'], [CardSubject::GROUP, CardSubject::ANONYMOUS])) {
            $firstLetter = '';
            if (strlen($data['last_name']) != 0) {
                $firstLetter = mb_substr($data['last_name'], 0, 1);
            } else {
                $firstLetter = mb_substr($data['first_name'], 0, 1);
            }
            $number = $firstLetter . '-' . $number ;
        }

        if ($data['admission_type_id'] == CardAdmissionType::PERSONAL_WELCOME) {
            $number = 'ОП-' . $number;
        }
        if ($data['admission_id'] == CardAdmission::UGL) {
            $number = $number . 'УГЛ';
        }
        if ($data['admission_id'] == CardAdmission::HEADS_HOT_LINE) {
            $number = $number . 'ГЛ';
        }
        if ($data['subject_id'] == CardSubject::GROUP) {
            $number = 'КО-'.$number;
        }
        if ($data['subject_id'] == CardSubject::ANONYMOUS){
            $number = 'АН-'.$number;
        }
        return $number;
    }

    private static function getNextId(array $data): int
    {
        $card = Card::orderBy('id', 'desc')->first();
        if (is_null($card)) return 1;
        preg_match('/^[а-яА-ЯїієґҐЇІЄ-]{1,5}-(\d+)\/10[А-Я]{0,5}/u', $card->number, $matches);
        $nextNumber = ((int) $matches[1]) + 1;
        return $nextNumber;
        $statement = DB::select('SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = "'.env('DB_DATABASE').'" AND TABLE_NAME = "cards"');
        if (is_null($statement[0]->AUTO_INCREMENT)){
            return 1;
        }else{
            $newAutoIncrement = $statement[0]->AUTO_INCREMENT;
            DB::statement("ALTER TABLE cards AUTO_INCREMENT = " . $newAutoIncrement . ";");
        }

        return $newAutoIncrement;
    }
    private static function getNumberForDouble(string $cardNumber){
        preg_match('/^[а-яА-ЯїієґҐЇІЄ-]{1,5}-\d+\/10[А-Я]{0,5}/u', $cardNumber, $matches);
        $cardNumber = $matches[0];
        $cards = Card::where('number', 'LIKE', $cardNumber . "%")
            ->orderBy('created_at', 'asc')
            ->get();
        $firstCard = $cards[0];
        //dd($firstCard);
        $firstCardNumber = $firstCard->number;
        $cardsCount = $cards->count();
        $newNumber = $firstCardNumber . '/'.$cardsCount;
        return $newNumber;
    }
}
