<?php

namespace App\Services\Card\CardManagement;

use Carbon\Laravel\ServiceProvider;

class CardManagementServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CardManagementService::class, function(){
            return new CardManagement();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
