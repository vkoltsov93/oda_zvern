<?php

namespace App\Services\Card\CardManagement;

use App\Services\Card\CardManagement\Actions\Store as CardStore;

class CardManagement implements CardManagementService
{

    public function store(array $data): bool
    {
        //dd($data);
        return CardStore::run($data);
    }

    public function update(array $data): bool
    {
        // TODO: Implement update() method.
    }
}
