<?php

namespace App\Services\Card\CardManagement\Actions;

use App\Models\Card;
use App\Models\Doer;
use App\Models\Question;
use App\Models\SubQuestion;
use App\Services\Card\CardManagement\Helpers\GenerateNumber;
use App\Services\Card\CardManagement\Helpers\MappingFromForm;
use Illuminate\Support\Facades\Auth;

class Store
{
    public static function run(array $data): bool
    {
        $mappedData = MappingFromForm::handle($data);
        $number = GenerateNumber::handle($mappedData);
        $mappedData['number'] = $number;
        $questionIds = $mappedData['question_id'];
        unset($mappedData['question_id']);
        $subQuestionIds = $mappedData['subquestion_id'];
        unset($mappedData['subquestion_id']);
        $doerIds = $mappedData['doer_id'];
        unset($mappedData['doer_id']);

        $mappedData['user_id'] = Auth::user()->id;
        $questions = [];

        foreach ($subQuestionIds as $questionId){
            $subQuestion = SubQuestion::find($questionId);
            $question = $subQuestion->question;
            $questions[] = $question->id;
        }

        $card = Card::create($mappedData);
        $doers = Doer::whereIn('id', $doerIds)->get();
        foreach ($doers as $doer){
            $card->doers()->attach($doer);
        }

        $subQuestions = SubQuestion::whereIn("id", $subQuestionIds)->get();
        foreach ($subQuestions as $subQuestion){
            $card->questions()->attach($subQuestion->question, ['sub_question_id' => $subQuestion->id]);
        }
        return true;
    }
}
