<?php

namespace App\Models;

use App\Models\Constants\Card\CardStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Card extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $dates = ['incoming_date'];

    protected $fillable = [
        'number',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'district_id',
        'union_community_id',
        'locality_id',
        'address',
        'zip',
        'phone',
        'phone_extra',
        'email',
        'social_status_id',
        'category_id',
        'corr_note',
        'sign_id',
        'type_id',
        'admission_type_id',
        'kind_id',
        'subject_id',
        'person_count',
        'incoming_date',
        'is_local',
        'country',
        'region',
        'district',
        'locality',
        'admission_id',
        'org_date',
        'org_index',
        'org_note',
        'appeal_text_note',
        'responsible_id',
        'responsible_id_close',
        'term',
        'resolution',
        'is_on_control',
        'is_closed',
        'user_id_closed',
        'user_id_set_on_control',
        'user_id',
        'closed_number',
        'closed_date',
    ];

    public function questions(){
        return $this
            ->belongsToMany(Question::class)
            ->withPivot(['sub_question_id']);
    }

    public function doers(){
        return $this->belongsToMany(Doer::class);
    }

    public function results(){
        return $this->belongsToMany(Result::class)
            ->withPivot([
                'doer_id',
                'question_id',
                'sub_question_id',
            ]);
    }

    public function userOpened(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function userSetOnControl(){
        return $this->belongsTo(User::class, 'user_id_set_on_control');
    }
    public function userClosed(){
        return $this->belongsTo(User::class, 'user_id_closed');
    }

    public function responsible(){
        return $this->belongsTo(Responsible::class, 'responsible_id');
    }
    public function responsibleClosed(){
        return $this->belongsTo(Responsible::class, 'responsible_id_close');
    }

    public function getStatus(){
        if (!($this->is_on_control) && !($this->is_closed)){
            return CardStatus::OPEN;
        }
        if ($this->is_on_control){
            return CardStatus::CONTROL;
        }
        if ($this->is_closed){
            return CardStatus::CLOSED;
        }
    }
}
