<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Responsible extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['name', 'is_active', 'responsible_position_id'];

    public function responsiblePosition(){
        return $this->belongsTo(ResponsiblePosition::class);
    }
}
