<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['name', 'organization', 'organization_to', 'email', 'order_index'];
}
