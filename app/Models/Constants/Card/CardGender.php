<?php


namespace App\Models\Constants\Card;


class CardGender
{
    public const MALE = 1;
    public const FEMALE = 0;
}
