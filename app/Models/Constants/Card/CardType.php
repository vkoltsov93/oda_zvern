<?php


namespace App\Models\Constants\Card;


class CardType
{
    public const LETTER = 1;
    public const ORAL = 2;
    public const TELEGRAM = 3;
}
