<?php

namespace App\Models\Constants\Card;

class CardCategory
{
    public const A_PARTICIPANT_IN_THE_WAR = 1;
    public const CHILD_OF_WAR = 2;
    public const INVALID_OF_THE_GREAT_PATRIOTIC_WAR = 3;
    public const WAR_INVALID = 4;
    public const A_PARTICIPANT_IN_HOSTILITIES = 5;
    public const VETERAN_OF_LABOR = 6;
    public const INVALID_I_GROUP = 7;
    public const INVALID_II_GROUP = 8;
    public const INVALID_III_GROUP = 9;
    public const THE_CHILD_IS_INVALID = 10;
    public const A_SINGLE_MOTHER = 11;
    public const MOTHER_IS_A_HEROINE = 12;
    public const LARGE_FAMILY = 13;
    public const A_PERSON_AFFECTED_BY_THE_CHERNOBYL_DISASTER = 14;
    public const PARTICIPANT_IN_THE_LIQUIDATION_OF_THE_CONSEQUENCES_OF_THE_CHERNOBYL_ACCIDENT = 15;
    public const HERO_OF_UKRAINE = 16;
    public const HERO_OF_THE_SOVIET_UNION = 17;
    public const HERO_OF_SOCIALIST_LABOR = 18;
    public const CHILD = 19;
    public const OTHER_CATEGORY = 20;
    public const WITHOUT_CATEGORY = 21;


}
