<?php


namespace App\Models\Constants\Card;


class CardStatus
{
    public const OPEN = 1;
    public const CONTROL = 2;
    public const CLOSED = 3;
}
