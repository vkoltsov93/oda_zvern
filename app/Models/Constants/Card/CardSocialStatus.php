<?php


namespace App\Models\Constants\Card;


class CardSocialStatus
{
    public const PENSIONER = 1;
    public const WORKER = 2;
    public const PEASANT = 3;
    public const BUDGET_SPHERE_EMPLOYEE = 4;
    public const CIVIL_SERVANT = 5;
    public const ENLISTEE = 6;
    public const ENTREPRENEUR = 7;
    public const UNEMPLOYED = 8;
    public const STUDENT = 9;
    public const RELIGIOS = 10;
    public const PRISONER = 11;
    public const WAR_PARTICIPANT = 12;
    public const CHILD_OF_WAR = 13;
    public const BIG_WAR_DISABLED_PERSON = 14;
    public const WAR_DISABLED_PERSON = 15;
    public const COMBATANT = 16;
    public const VETERAN_OF_LABOUR = 17;
    public const DISABLED_PERSON_GROUP_1 = 18;
    public const DISABLED_PERSON_GROUP_2 = 19;
    public const DISABLED_PERSON_GROUP_3 = 20;
    public const DISABLED_PERSON_CHILD = 21;
    public const SINGLE_MOTHER = 22;
    public const HERO_MOTHER = 23;
    public const LARGE_FAMILY = 24;
    public const CHERNOBOL_VICTIM = 25;
    public const CHERNOBOL_LIQUIDATOR = 26;
    public const UKRAINE_HERO = 27;
    public const ATO_PARTICIPANT = 28;
    public const ATO_PARTICIPANT_RELATIVE = 29;
    public const CHILD = 30;
    public const RELATIVE_DEAD_MILITARY = 31;
    public const OTHER = 32;
    public const WITHOUT_SOCIAL_STATUS = 33;
}
