<?php


namespace App\Models\Constants\Card;


class CardSign
{
    public const FIRST = 1;
    public const REPEATED = 2;
    public const DOUBLE = 3;
}
