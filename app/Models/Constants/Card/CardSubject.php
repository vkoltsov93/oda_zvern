<?php


namespace App\Models\Constants\Card;


class CardSubject
{
    public const INDIVIDUAL = 1;
    public const GROUP = 2;
    public const ANONYMOUS = 3;
}
