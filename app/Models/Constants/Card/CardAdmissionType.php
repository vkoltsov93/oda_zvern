<?php


namespace App\Models\Constants\Card;


class CardAdmissionType
{
    public const POST = 1;
    public const PERSONAL_WELCOME = 2;
    public const AUTHORITIES = 3;
    public const MEDIA = 5;
    public const OTHER = 6;
    public const AUTHORIZED_PERSON = 7;
}
