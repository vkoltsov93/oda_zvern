<?php


namespace App\Models\Constants\Card;


class CardAdmission
{
    public const LOCAL = 1;
    public const PRESIDENT = 2;
    public const PRESIDENTS_OFFICE = 3;
    public const UGL = 4;
    public const SECRETARIAT_KMU = 5;
    public const STAFF_OF_THE_VERKHOVNA_RADA = 6;
    public const SECRETARIAT_OF_THE_UKRAINIAN_PARLIAMENT_COMMISSIONER_FOR_HUMAN_RIGHTS = 7;
    public const COMMISSIONER_FOR_THE_RIGHTS_OF_THE_PRESIDENT_OF_UKRAINE = 8;
    public const EMAIL = 9;
    public const MINISTRY_OF_DEVELOPMENT_OF_COMMUNITIES_AND_TERRITORIES = 10;
    public const REGIONAL_COUNCIL_ZT = 11;
    public const OFFICE_OF_THE_ATTORNEY_GENERAL = 12;
    public const NATIONAL_SOCIAL_SERVICE = 13;
    public const GENERAL_PROSECUTOR = 14;
    public const HEADS_HOT_LINE = 15;
    public const MINISTRY_OF_SOCIAL_POLICY = 16;
    public const MINISTRY_OF_INTERNAL_AFFAIRS = 17;
    public const OTHER_AUTHORITIES = 18;
    public const PRIME_MINISTER = 19;
    public const PEOPLES_DEPUTY = 20;
    public const CHAIRMAN_OF_THE_VERKHOVNA_RADA = 21;
    public const MINISTRY_OF_HOUSING = 22;
    public const MINISTRY_OF_ECOLOGY_AND_NATURAL_RESOURCES = 23;
    public const MINISTRY_OF_ECONOMY = 24;
    public const MINISTRY_OF_FUEL_AND_ENERGY = 25;
    public const MINISTRY_OF_COAL_INDUSTRY = 26;
    public const MINISTRY_OF_INTERNATIONAL_AFFAIRS = 27;
    public const MINISTRY_OF_CULTURE_AND_TOURISM = 28;
    public const MINISTRY_OF_EMERGENCY_SITUATIONS = 29;
    public const MINISTRY_OF_DEFENCE = 30;
    public const MINISTRY_OF_EDUCATION_AND_SCIENCE = 31;
    public const MINISTRY_OF_HEALTH = 32;
    public const MINISTRY_OF_INDUSTRIAL_POLICY = 33;
    public const MINISTRY_OF_INFRASTRUCTURE = 34;
    public const MINISTRY_OF_FAMILY_YOUTH_AND_SPORTS = 35;
    public const MINISTRY_OF_FINANCE = 36;
    public const MINISTRY_OF_JUSTICE = 37;
    public const PENSION_FUND = 38;
    public const MEDIA = 39;
    public const CABINET_OF_MINISTERS = 40;
    public const VERKHOVNA_RADA = 41;
    public const OTHER_APPARATUS = 42;
    public const STATE_MIGRATION_SERVICE = 43;
    public const NATIONAL_ANTI_CORRUPTION_BUREAU = 44;
    public const REGIONAL_COUNCIL = 45;
    public const MINISTRY_OF_VETERANS_AFFAIRS = 46;
    public const ZHYTOMYR_REGIONAL_PROSECUTORS_OFFICE = 47;
}
