<?php


namespace App\Models\Constants\Card;


class CardKind
{
    public const APPLICATION = 1;
    public const COMPLAINT = 2;
    public const SUGGESTION = 3;
}
