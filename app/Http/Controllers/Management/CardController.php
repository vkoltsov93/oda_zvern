<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Models\Card;
use App\Models\Locality;
use App\Models\Question;
use App\Models\SubQuestion;
use App\Services\Card\CardManagement\CardManagementService;
use App\Services\Card\CardProperties\CardPropertiesService;
use Illuminate\Http\Request;

class CardController extends Controller
{

    private $cardPropertyService;

    function __construct(CardPropertiesService $cardPropertiesService) {
        $this->cardPropertyService = $cardPropertiesService;
    }

    public function create()
    {
        $cardProperties = $this->cardPropertyService->getAll();
        return view('management.card.create.index')->with($cardProperties);
    }

    public function store(Request $request, CardManagementService $cardManagementService)
    {
        $data = $request->all();
        $cardManagementService->store($data);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function getSubquestions(){
        $questionId = (int) \request('question_id');
        $rowNumber = \request('row_id');

        $subquestions = $this
            ->cardPropertyService
            ->getSubQuestions()
            ->where('question_id', $questionId);

        $subQuestionsData = [
            'subquestions' => $subquestions,
            'rowNumber' => $rowNumber,
        ];

        if (!is_null(\request('subquestion_id'))){
            $subQuestionsData['cardSubQuestionId'] = \request('subquestion_id');
        }

        $html = view('management.card.create.partials.sub_questions')->with($subQuestionsData)->render();

        $data = [
            'success' => true,
            'html' => $html,
            'rowNumber' => $rowNumber,
        ];

        return response()->json($data);
    }

    public function getQuestions(){
        $rowNumber = (int) \request('rowNumber');
        ++$rowNumber;

        $questions = $this->cardPropertyService->getQuestions();

        $questionsData = [
            'questions' => $questions,
            'rowNumber' => $rowNumber,
        ];

        if (!is_null(\request('question_id'))){
            $questionsData['cardQuestionId'] = (int) \request('question_id');
        }

        $html = view('management.card.create.partials.question_row')
            ->with($questionsData)
            ->render();

        return response()->json([
            'success' => true,
            'html' => $html,
            'rowNumber' => $rowNumber,
        ]);
    }

    public function getDoers(){
        $rowNumber = (int) (int) \request('rowNumber');
        ++$rowNumber;

        $doers = $this->cardPropertyService->getDoers();
        $htmlData = [
            'doers' => $doers,
            'rowNumber' => $rowNumber,
        ];

        if(!is_null(\request('doer_id'))){
            $htmlData['cardDoerId'] = (int) \request('doer_id');
        }

        $html = view('management.card.create.partials.doer_row')
            ->with($htmlData)
            ->render();

        return response()->json([
            'success' => true,
            'html' => $html,
            'rowNumber' => $rowNumber,
        ]);
    }

    public function getUnionCommunities(){
        $districtId = (int) request('district_id', 1);
        $otgs = $this
            ->cardPropertyService
            ->getUnionCommunities()
            ->where('district_id', $districtId);

        $htmlData = [
            'otgs' => $otgs,
        ];
        if(!is_null(\request('otg_id'))){
            $htmlData['cardOtgId'] = \request('otg_id');
        }
        $html = view('management.card.create.partials.otgs')->with($htmlData)->render();


        return response()->json([
            'success' => true,
            'html' => $html,
        ]);
    }

    public function getLocalities(){
        $otgId = request('otg_id', 1);

        $localities = $this
            ->cardPropertyService
            ->getLocalities()
            ->where('union_community_id', $otgId);

        $htmlData = [
            'localities' => $localities,
        ];
        if(!is_null(\request('locality_id'))){
            $htmlData['cardLocalityId'] = \request('locality_id');
        }
        $html = view('management.card.create.partials.localities')->with($htmlData)->render();

        return response()->json([
            'success' => true,
            'html' => $html,
        ]);
    }

    public function list(){
        $cards = Card::orderByDesc('id')
            ->with('responsible')
            ->paginate(10);
        return view('management.card.list.index')->with('cards', $cards);
    }

    public function destroy($id)
    {
        //
    }

}
